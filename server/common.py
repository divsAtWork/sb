import struct
import base64
import hashlib
import json
import traceback
from pymongo import MongoClient
import copy
import os
#### URLS ####
UPLOAD_API = '/upload'
LOGIN_API = '/login'
LOGOUT_API = '/logout'
REGISTER_CLIENT_API = '/regcli'
USER_LIST_API = '/userlist'
FILELIST_API = '/filelist'
DOWNLOAD_API = '/download'
REGISTER_USER_API = '/register'
USERHOME_API = '/userhome'
ADMINHOME_API = '/adminhome'
ADMIN_STORAGE_API = '/storage'
GET_POLICY_API = '/getpolicy'
DEVICE_API = '/devices'
SETTINGS_API = '/settings'
GROUP_API = '/groups'
########### Other Variables ########
USER = 'user'
ADMIN = 'admin'
STORAGE_INACTIVE = 'inactive'
STORAGE_ACTIVE = 'active'
STORAGE_REMOVED = 'removed'
BACKUP_INACTIVE = 'inactive'
BACKUP_ACTIVE = 'active'
BACKUP_REMOVED = 'removed'
LOG_CONFIG_KEY = 'LOG'
LOG_LEVEL_CONFIG_KEY = 'level'
LOG_LOCATION_CONFIG_KEY = 'location'
LOG_FILENAME = 'log.txt'
########### Directories ########
CONFIG_DIR = ".."
CONFIG_FILE = "config.ini"
PRODUCT_NAME = "simandhar"
########## CONFIGURATION Numbers #####
MAX_UPLOAD_RETRY = 5
MEMBER_TYPE = ['User','Admin']
if os.name=='nt':
    PID_FILE = "../../"+PRODUCT_NAME+".pid"
else:
    PID_FILE = '/var/run/'+PRODUCT_NAME+'.pid'

global ldapobj
ldapobj = None

#Data sizes
KB = 1024
MB = 1024*KB
GB = 1024*MB
min_chunking_limit = 20*MB


#Other defaults
mybase = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_=+?@$*"



import configparser
import os

def init_config():
    global config_inited
    try:
        if config_inited:
            return config
    except:
        config = configparser.ConfigParser()
        config.read(os.path.join(CONFIG_DIR, CONFIG_FILE))
        if config.sections() == []:
            raise ValueError('No config file provided.')
        config_inited = True
        return config

config = init_config()

def get_original_filename(tmpPath):
    #Original File name was appeneded with 5 digits random number and timestamp
    return tmpPath.split()[:10]


def get_save_to_path():
    upath = config.get('TEMP', 'upload_temp_location')
    if upath == None:
        return ".."
    return upath

def get_download_cache_path():
    dpath = config.get('TEMP', 'download_cache_location')
    try:
        os.makedirs(dpath, exist_ok=True)
    except:
        dpath = None
    if dpath is None:
        tmpDir = os.getenv('TEMP') or os.getenv('TMP')
        tmpDir = os.path.join(tmpDir, PRODUCT_NAME, 'download_cache')
        return tmpDir
    return dpath

def zip_files_at_path(folder, zipname):
    import zipfile
    with zipfile.ZipFile('zipname', 'w') as myzip:
        parent = folder
        for aFile in os.listdir(folder):
            if os.path.isfile(os.path.join(parent, aFile)):
                myzip.write(aFile)
            else:
                parent = os.path.join(parent, aFile)
    return zipname


import web
global session
session = None

def size_str(num, decimalPoints):
    size_acronym = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'NB', 'PB']
    index = 0
    num = float(num)
    prevnum = num
    while num > 0.5:
        prevnum = num
        num = round((float(num) / 1024.0), 2)
        index += 1
    if not index:
        index = 1
    return str(round(prevnum, decimalPoints)) + " " + size_acronym[index - 1]

#assumed size_str like 5KB or 5 KB or 2.5 TB
def get_byte_from_value_str(size_str):
    # Remove white spaces if any
    size_str = size_str.replace(' ', '')
    # now we are assming in format only like 5KB or 5000
    numStr = ""
    negative_value = False
    if size_str[0] == '-':
        negative_value=True
        size_str = size_str[1:]
    for x in size_str:
        try:
            numStr += str(int(x))
        except:
            break
    print(size_str.split(numStr))
    saved_numStr = numStr
    if len(size_str.split(numStr)) == 2:
        # we got some characters
        chars = (size_str.split(numStr)[1]).lower()
        size_map = ("", "kb", "mb", "gb", "tb", "nb", "pb", "none")
        for x in size_map:
            if x == chars:
                break;
            else:
                numStr = int(numStr) * 1024
        if x == "none":
            numStr = saved_numStr
        # we may have got single characters
            chars = (size_str.split(numStr)[1]).lower()
            size_map = ("", "k", "m", "g", "t", "n", "p", "none")
            for x in size_map:
                if x == chars:
                    break;
                else:
                    numStr = int(numStr) * 1024
            if x == "none":
                return -1
    if negative_value:
        return (0-int(numStr))
    return int(numStr)

def get_file_hash(fname):
    hash_sha256 = hashlib.sha256()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_sha256.update(chunk)
    return hash_sha256.hexdigest()


def key2cnt(keystr):
    keydata = decr_data(keystr)
    #keydata = keydata.decode('utf-8')
    keydata = json.loads(keydata)
    if keydata["h"]==hashlib.md5((keydata["Organization"] + " " + keydata["count"]).encode('utf-8')).hexdigest():
        return int(keydata["count"])
    return 0









































def encr_data(data):
    #seedlocations = [7,17,29]
    retval = []
    counter=1
    for x in data:
        #for p in seedlocations:
        #    if(counter%p)==0:
        #       n = random.randrange(1, 29)
        #        print("+@",counter,":",n)
        #        #retval.append(chr(97 + random.randrange(0, 25)))
        retval.append(ord(x) ^ 30)
        #retval.append(x)
        counter +=1
    retval=struct.pack("b"*len(retval), *retval)
    retval= base64.b64encode(retval)
    #retval = base64.b64encode(retval.encode())
    return retval

def decr_data(data):
    #seedlocations = [7, 17, 29]
    packed_data = base64.b64decode(data)
    s = struct.Struct('b'*len(packed_data))
    unpacked_data = s.unpack(packed_data)
    retval = []
    counter = 1
    #drop = False
    for x in unpacked_data:
        #if drop:
        #    #counter+=1
        #    drop=False
        #    continue
        #for p in seedlocations:
        #    if (counter % p) == 0:
        #        if drop:
        #            print("dropping already dropped")
        #        drop=True
        #        print("-@",counter,":",x)
        #        #retval.append(random.randrange(1, 29))
        #        # retval.append(chr(97 + random.randrange(0, 25)))
        #if not drop:
        retval.append(chr(x^30))
        # retval.append(x)
        counter += 1
    retval = ''.join(retval)
    return retval


