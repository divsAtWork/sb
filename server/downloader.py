import download
import time
import mylog
from zipfile import ZipFile

mylog.log = mylog.Log()
log = mylog.log
log.set_log_file_name('../logs/download.log')
log.start()
log.info("Downloader Service Started...")
d = download.Downloader()
d.set_default_download_source()
while 1:
    d.process_download_queue()
    time.sleep(10)