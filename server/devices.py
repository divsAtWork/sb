import dbwrapper
import pymongo
import user, sys
import web


render = web.template.render('templates/', base="layout")

def validate_admin(someone, *args):
    def wrapper(*args):
        try:
            session = web.config.session
            print("Session Count = ", session['count'])
            if session['count'] >= 1:
                if session.get('utype', 'user')== 'admin':
                    return someone(*args)
            else:
                return web.seeother('/index')
        except IOError as e:
            print("I/O error({0}): {1}".format(e.errno, e.strerror))
        except ValueError:
            print("Could not convert data to an integer.")
        except:
            print("Unexpected error:", sys.exc_info()[0])
        return web.seeother('/index')
    return wrapper

class Devices:
    def __init__(self):
        self.db = dbwrapper.ServerDB()
        self.db2 = self.db.get_db()
        self.table = self.db2.devices
        self.myuser = user.User(self.db2)

    @validate_admin
    def GET(self):
        return render.devices(devices=self.check(), msg='')

    #@validate_admin
    def POST(self):
        i = web.input()
        return self.GET()

    def check(self):
        result = self.table.find()
        devList = []
        for res in result:
            uname = self.myuser.get_user_details(int(res['uid']))['name']
            devList.append({'username':uname, 'devicename':res['name'], '_id':res['_id'], 'datasize':res['size']})
        return devList