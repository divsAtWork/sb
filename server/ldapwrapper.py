from ldap3 import Server,Connection,ALL, NTLM, ALL_ATTRIBUTES
import dbwrapper as dbwrapper
import json,re
from common import *

class LdapWrapper:
    def __init__(self):
        try:
            self.db = dbwrapper.ServerDB()
        except:
            self.db = None

    def add_ldap_settings(self, web_input):
        inputs = json.loads(web_input)
        settings = {
            'name': inputs['name'],
            'IP': inputs['IP'],
            'port': int(str(inputs['port'])),
            'attribs':{
                "login_attrib":str(inputs['login_attrib']),
                "display_name_attrib": inputs['display_name_attrib']
                },
            'base_dn': str(inputs['base_dn']),
            'manager_dn': inputs['manager_dn'],
            'password': encr_data(inputs['passwd'])
            }
        return self.db.add_ldap_settings(settings)

    def get_ldap_settings(self, name):
        if name == None:
            return self.db.get_ldap_settings(name)
        else:
            result = self.db.get_ldap_settings(name)
            return {
            'name': result['name'],
            'IP': result['IP'],
            'port': int(str(result['port'])),
            'attribs':{
                "login_attrib": (result['attribs']['login_attrib']),
                "display_name_attrib": result['attribs']['display_name_attrib']
                },
            'base_dn': (result['base_dn']),
            'manager_dn': result['manager_dn'],
            'password': decr_data(str(result['password']))
            }

    def edit_ldap_settings(self, settings):
        changed_settings = json.loads(settings)
        result = self.get_ldap_settings(changed_settings['name'])
        if result['IP'] != changed_settings['IP']:
            self.db.ldap_settings.update({"name": str(settings['name'])}, {'$set': {'IP': str(changed_settings['IP'])}})
        if result['base_dn'] != changed_settings['base_dn']:
            self.db.ldap_settings.update({"name": str(settings['base_dn'])}, {'$set': {'base_dn': str(changed_settings['base_dn'])}})
        #TODO: Complete updating of ldap setting

    def delete_ldap_settings(self, name):
        self.db.remove_ldap_settings(name)

    def test_connection(self, domain, port, username, password):
        server = Server(port=port, host=domain, get_info=ALL)
        conn = Connection(server, user=username, password=password, authentication=NTLM)
        try:
            return True if conn.bind() else False
        except:
            return False

    def get_base_dn(self, domain, port=389):
        server = Server(domain, port=port, get_info=ALL)
        conn = Connection(server)
        return server.info.naming_contexts[0] if conn.bind() else ""

    def get_groups_name(self, name):
        setting = self.get_ldap_settings(name)
        server = Server(setting['IP'], get_info=ALL)
        conn = Connection(server, user=setting['manager_dn'], password=setting['password'], authentication=NTLM)
        try:
            if conn.bind():
                conn.search(str(setting['base_dn']), search_filter='(objectClass=Group)')
                result = list(conn.entries)
                if len(result) == 0:
                    conn.unbind()
                    return "No groups found"
                else:
                    regex_short = re.compile('.+(CN=Builtin).+|.+(CN=Users).+')
                    tmp=[]
                    for i, j in enumerate(result):
                        if re.match(regex_short, str(j)):
                            print(i)
                        else:
                            tmp.append(str(result[i]).split(',')[0][7:])
                    conn.unbind()
                    return tmp
            else:
                conn.unbind()
                return "Connection could not be established"
        except:
            conn.unbind()
            return ['1','2','3']

    def import_group(self, name, setting_name):
        result = self.get_ldap_settings(setting_name)
        search_base='dc='+str(name)+str(result['base_dn'])
        server = Server(result['IP'], get_info=ALL)
        conn = Connection(server, user=result['manager_dn'], password=result['password'], authentication=NTLM)
        try:
            if conn.bind():
                conn.search(search_base=search_base, search_filter='(objectClass=group)')
                grp = conn.entries[0]
                max_id = int(str(self.db.get_groups(None, True)))
                data = {"gid": max_id, "type":"ad", "name": str(grp.name), "dn":str(grp.dn), "ldap_setting":name}
                self.db.add_group(data)
                return True
            else:
                #no connection could be established that's whhy
                return False
        except:
            #connection issue
            return False