from pymongo import MongoClient, DESCENDING
import copy
import sys, time,os
from common import *
import json
import user
import urllib
import random
import base64
import traceback
import re
from storewrapper import Folder
class ServerDB:

    def __init__(self):
        try:
            self.db = MongoClient('localhost', 27017).get_database(PRODUCT_NAME)
            self.user = user.User(self.db)
            self.files = self.db.files
            self.devices = self.db.devices
            self.storage = self.db.storage
            self.ldap_settings = self.db.ldap_settings
            self.groups = self.db.groups
            self.reports = self.db.reports

        except IOError as e:
            print("I/O error({0}): {1}".format(e.errno, e.strerror))
        except ValueError:
            print("Could not convert data to an integer.")
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise

    def get_db(self):
        return self.db

    def get_next_device_id(self):
        try:
            result = self.db.dev_idx.find()
            for r in result:
                self.db.dev_idx.update({},{'count':r['count']+1})
                return r['count']+1
            self.db.dev_idx.insert_one({'count': 1})
            return 1
        except:
            self.db.dev_idx.insert_one({'count':1})
            return 1

    def get_device_id(self, session):
        try:
            devId = session['devid']
            if devId != 0:
                return devId
        except:
            pass
        devId = 0
        uid = self.user.get_user_id(session)

        result = self.db.devices.find_one({'uid':uid, 'name':'online'},{'devid':1})
        if result != None:
            devId = result['devid']
        '''
        else:
            devId = self.get_next_device_id()
            self.db.devices.insert_one({'uid': int(uid), 'name': 'online', 'devid': int(devId), 'size':int(0)})
        '''
        session['devid'] = devId
        return devId

    def register_new_device(self, uid, name):
        result = self.db.devices.find_one({'uid': uid, 'name': 'online'}, {'devid': 1})
        if result != None:
            #Ideally you should never be here
            retval = {"devid":list(result)['devid'],"key":list(result)['key']}
            return list(result)['devid'], retval
        else:
            devId = self.get_next_device_id()
            key = str(random.getrandbits(256))
            self.db.devices.insert_one({'uid': int(uid), 'name': name, 'devid': int(devId), 'key':key, 'size':int(0)})
            return devId, encr_data(str({"devid":devId, "key":key}))

    def get_policies(self, uid):
        pattern = 'N'+str(uid)+'N'
        import re
        regex = re.compile(pattern, re.IGNORECASE)
        result = self.db.policies.find({'uids':regex})
        if result != None:
                target = []
                for ret in result:
                    #Remove leading / before sending response.
                    target.append(ret["target"].replace('/','',1))
                return {"target":target}
        else:
            return {"target":""}

    def create_policy(self, uid, target):
        pattern = 'N' + str(uid) + 'N'
        self.db.policies.insert_one({'uids':pattern, "target":target})
        return "OK"

    def get_version_download_link(self, id):
        retval =  "/download/version/"+id
        #retval = retval.replace('//', '/')
        retval = urllib.parse.quote(retval)
        return retval

    # Return not only list of files on given path, returns versions of a file (parent = file)
    # Also return if the returned list is of versions and not files.
    # So versions = True, if its a version's list
    def get_file_list(self, parent, devid):
        filelist = []
        files = self.db.files.distinct('path', {"parent":parent, "devid":devid})
        file_tuple_list = []
        if files==filelist:
            vers = []
            #probably we hit a file. So see if we have versions for it.
            path,filename=parent.rsplit('/', 1)
            versions = self.db.files.find({'parent':path, 'path':'/'+filename,"devid":devid },{'version':1, '_id':1})
            for ver in versions:
                version_as_time = time.strftime('%d-%b-%Y %H:%M:%S', time.localtime(int(ver["version"])))
                download_link = self.get_version_download_link(str(ver['_id']))
                file_tuple_list.append((version_as_time, download_link))#   +'? Version: ' + version_as_time)
            #Return versions list
            return file_tuple_list, True
        else:
            #Its a list of files. Lets prepare download button link for this
            for f in files:
                download_link = "/download"+urllib.parse.quote(parent+f)
                #make tuple
                file_tuple_list.append((f, download_link))
        #Return files list
        return file_tuple_list, False

    def get_macro_location_list(self, macro, devid):
        filelist = []
        regex = re.compile("/"+macro+"$", re.IGNORECASE)
        files = self.db.files.distinct('parent', {"parent": regex, "devid": devid})
        for f in files:
            filelist.append(f.split('/',1)[1]) #Removed starting '/'
        return filelist

    def Deprecated_get_users_list(self):#Return User list
        users = self.db.user.find({},{"name":1})
        userlist = []
        print([userlist.append(document["name"]) for document in users])
        return userlist

    def queue_file_for_upload(self, idx,  hsh):
        return self.db.uploadqueue.insert_one({"_id":idx, "hash":hsh, "stored":[0]})

    def query_and_create_path_entries(self, path, hsh, devid, mtime, filesize):
        #Sanitize Path expected i.e. only with / as separator.
        #initialPath = copy.deepcopy('/')
        try:
            if path == '/':
                return 0
            if '\\' in path:
                path = path.replace('\\', '/')
            if not path.startswith('/'):
                path = '/'+path
            parent, fpath = path.rsplit('/', 1)
            if parent == '':
                parent = '/'
        except:
                return 0
        try:
            result = self.db.files.find_one({"parent":parent, 'devid':devid})
            if result==None:
                raise
            filename = path.split(parent)[1]
            result = self.db.files.find({"parent":parent, "path": filename, 'devid':devid})
            print("Found same file  =",result.count())
            self.db.files.insert_one({"parent":parent, "path": filename, "hsh":hsh, "devid":devid, "version":mtime , "size":filesize, "store":"0"})
            self.db.devices.update({"devid":int(devid)}, {"$inc":{"size":filesize}}, upsert=False)
            return result.count()
        except:
            nparent = copy.deepcopy(parent)
            print("Need to create parent =", parent)
            version = self.query_and_create_path_entries(nparent, [], devid, mtime, filesize)
            print("version number returned =", version)
            try:
                filename = path.split(parent)[1]
            except:
                parent = '/'
                filename = path
                self.db.devices.update({"devid": int(devid)}, {"$inc": {"size": filesize}}, upsert=False)


            cursor = self.db.files.find({"parent":parent, "path": filename, "devid":devid})
            global config
            max_allowed_versions = int(config.get('VERSION_CONTROL', 'Max_allowed_versions'))
            if cursor.count() >= max_allowed_versions:
                #get the file list.. add the oldest version in the queue and then save the new version
                file_list = list(cursor.sort('version', pymongo.DESCENDING))
                self.add_to_queue('file_cleaner_queue', file_list[-1])
            self.db.files.insert_one(
                {"parent": parent, "path": filename, "hsh": hsh, "devid": devid, "version": mtime, "size": filesize,
                 "store": "0"})
            return 0

    def get_storage_list(self):
        try:
            result = self.db.storage.find({'stat':{'$ne':STORAGE_REMOVED}})
            if result != None:
                storage_set = []
                for stor in result:
                    #Before sendong to UI for human display, change bytes into human friendly notation
                    stor['settings']['size']=size_str(stor['settings']['size'], 2)
                    stor['settings']['usage'] = size_str(stor['settings'].get('usage',0), 2)
                    storage_set.append(stor) #[stor['_id']] = stor
                return storage_set
            else:
                return {}
        except:
            return {}

    def create_storage(self, settings):
        if (settings != None) and (settings!= {}):
            #Design assumption: Storage entries will be few (1-10) so we will never delete them.
            #If need to hide from display, we will use stat flag instead.
            try:
                id = int(self.db .storage.find().count())+1
            except:
                id = 1
            result = self.db.storage.insert_one({'_id':id, 'settings':settings, 'stat':STORAGE_INACTIVE})
            return result
        else:
            return None

    def set_storage_state(self, id, newState):
        result = self.db.storage.update({'_id': int(id)}, {'$set':{'stat': newState}})
        if result == None:
            return False
        return True

    def get_stored_path(self, devid, filepath, version):
        if filepath==None or filepath == '': return ""
        if "\\" in filepath:
            filepath = filepath.replace("\\", "/")
            parent, filename = filepath.rsplit("/", 1)
        retval = {}
        result = self.db.files.find_one({'parent':parent, 'path':filename, 'devid':devid})
        for res  in result:
            stored = res['stored']
            storages = self.get_storage_list()
            for aStore in storages:
                if aStore["_id"] == stored:
                    retval["type"] = aStore["type"]
                    retval["path"] = aStore["path"]+"/"+res["hsh"]
        return filename, retval

    def initiate_storage_migration(self, i):
        print("Received call for migration")
        print("From -",i.fram," To -", i.to)
        return True

    def get_all_files(self, tmpPath, dev_id):
        #First see if its a tmpPath of a file only
        #need to return actual path, actual file name, tmpFileName for all files as list or tuples
        result = self.db.files.find({'path':tmpPath, 'devid':dev_id})
        if list(result):
            result = list(result)
            return [(result['parent'], result['path'], tmpPath)]
        # This tmpPath is not of a file but actual path
        # so lets iterate for this and get actual tmpPaths of each file
        list_so_far = []
        def recursive_get_list(apath):
            result = self.db.files.find({'parent':apath, 'devid':dev_id})
            items = list(result)
            if not items:
                #It may be a file name
                parent,filename = apath.rsplit('/', 1)
                result = self.db.files.find({'parent': parent, 'path':'/'+filename, 'devid': dev_id})
            for aItem in items:
                if aItem['hsh']=='':
                    # Its a folder. Iterate...
                    recursive_get_list(aItem['parent']+aItem['path'])
                else:
                    #Its a file...
                    list_so_far.append((aItem['parent'],aItem['path'], aItem['hsh']))
            return list_so_far
        recursive_get_list(tmpPath)
        return list_so_far

    def deny_user(self, user_id):
        #TODO: send email if removed
        if user_id is None:
            print ("User_id pass null")
            pass
        try:
            self.db.user.remove({"name":user_id})
            return True
        except:
            return False

    def get_device_list_for_user(self, uid):
        result  = self.devices.find({"uid":int(uid)})
        devices = []
        if result != None:
            for aDev in result:
                devices.append({"name":aDev["name"], "id":aDev["devid"]})
        return devices

    def get_user_data_size(self, uid):
        result = self.db.devices.find({'uid':uid},{'size':1})
        consumption = 0
        for x in result:
            consumption += x['size']
        return consumption

    #Verify if we already have a file with same hash
    def is_duplicate(self, hsh, filesize):
        try:
            retval = self.db.hashtable.find_one({'_id':hsh})
            if retval!=None and list(retval)['size']!= filesize:
                return list(retval)['size']
            else:
                return None
        except:
            return None

    def file_to_upload(self, params, devid):
        params = json.loads(params)
        hashes = params['hashes'].split(",")
        self.db.expectedfiles.insert_one({'devid':devid, 'path': params['nm'], 'version':params['version'], 'size':params['size'] ,'hashes':hashes})
        chunks_needed = []
        for hsh in hashes:
            try:
                if not list(self.db.hashtable.find_one({'_id':hsh})):
                    chunks_needed.append(hsh)
            except:
                chunks_needed.append(hsh)

        return ','.join(chunks_needed)

    def increment_idx(self, i):
        if i == None or i == '':
            i = str(mybase[0])
        i = str(i)
        a = i[-1:]
        b = mybase[-1:]
        if a == b :
            remstr = i[:-1]
            return self.increment_idx(remstr) + mybase[0]
        else:
            loc = mybase.index(i[-1:])
            i = i[:-1] + mybase[loc + 1]
            return i

    def get_next_idx(self):
        try:
            res = list(self.db.hash_idx.find({'_id':'0'}))[0]['idx']
            res = self.increment_idx(res)
            self.db.hash_idx.update({'_id':'0'}, {'_id':'0', 'idx':res})
            return res
        except:
            traceback.print_exc()
            self.db.hash_idx.insert_one({'_id':'0', 'idx':'1'})
            return '1'

    def get_hash_list(self, params, devid):
        params = json.loads(params)
        res = self.db.expectedfiles.find_one({'devid':devid, 'path': params['nm'], 'version':params['version']}, {'hashes':1})
        if res:
            res = res['hashes']
            hashlist = []
            for hsh in res:
                result = self.db.hashtable.find_one({'_id': hsh}, {'idx': 1})
                if result != None:
                    idx = result['idx']
                    hashlist.append(idx)
                else:
                    return None
            return hashlist
        else:
            return None

    def del_hash_list(self, params, devid):
        return self.db.expectedfiles.remove({'devid': devid, 'path': params['nm'], 'version': params['version']})

    def add_hash(self, hsh, filesize):
        try:
            idx = self.get_next_idx()
            self.db.hashtable.insert_one({'_id': hsh, 'count': 1, 'idx':idx, 'size': filesize, 'store':[0,]})
            return idx
        except:
            #if dict(self.db.hashtable.find({'_id': hsh}))['size'] != filesize:
                #Exceptional condition. Different sized files/chunks have same hash
            #    return None
            #else:
            self.db.hashtable.update({'_id': hsh}, {'$inc': {'count': 1}})
            idxdict = self.db.hashtable.find_one({'_id':hsh},{'idx':1})
            idx = idxdict["idx"]
            return idx

    def remove_hash_from_store(self, hsh, stores):
        for store in stores:
            store = self.db.storage.find_one({"_id": store})
            if store['settings']['type'] == 'disk':
                storePointer = Folder(store['settings']['folder'])
                return storePointer.remove_from_store(hsh)

    def del_hash(self, hsh):
        try:
            retval = list(self.db.hashtable.find({'_id': hsh}, {'count': 1, 'store':1}))[0]
            cur_count = retval['count']
            if cur_count == 1:
                #Need to delete from disk too
                self.remove_hash_from_store(hsh, retval['store'])
                self.db.hashtable.remove({'_id': hsh})
            else:
                self.db.hashtable.update({'_id': hsh}, {'$dec': {'count': 1}})
        except:
            pass

    def check_chunk_hash(self, hsh):
        x = self.db.hashtable.count({'_id': hsh})
        if x != 0:
            return True
        return False

    def get_storage_details(self, id):
        try:
            result = self.db.storage.find_one({"_id": int(id)})
        except:
            result = {}
        return result

    def add_ldap_settings(self, settings):
        try:
            self.ldap_settings.insert_one(settings)
            return True
        except:
            return False

    def get_ldap_settings(self, name=None):
        try:
            if name == None:
                results = self.db.ldap_settings.find()
                settings = []
                for result in results:
                    settings.append(result)
                return settings
            else:
                return self.db.ldap_settings.find_one({"name":name})
        except:
            return []

    def remove_ldap_settings(self, name):
        try:
            self.db.ldap_settings.remove({"name":str(name).strip()})
        except:
            pass

    def get_groups(self, id, max_id=False):
        try:
            if id == None and max_id == False:
                return self.groups.find()
            elif max_id == False and id != None:
                return self.groups.find_one({"gid":int(id)})
            else:
                try:
                    return self.groups.find().sort("gid", -1).limit(-1)[0]['gid']
                except:
                    return 0
        except:
            return {}

    def add_group(self, data):
        try:
            self.groups.insert_one(data)
            return True
        except:
            return False

    def delete_group(self, id):
        try:
            if int(id):
                self.groups.remove({"gid":int(id)})
        except:
            pass

    def add_to_queue(self, queue_name, data):
        '''
        Method to add the data into the queue with the db object.
        :param queue_name: collection name for queue
        :param data: The data which is to be put into the queue
        '''
        if type(queue_name) is not str:
            print("queue name not valid: ", queue_name)
            return
        queue_collection = self.db[str(queue_name)]
        try:
            queue_collection.insert_one(data)
        except:
            print ("Error while adding data:[%s] into the queue: [%s]" %data %str(queue_name))