#!/bin/sh
if test "$#" -ne 1; then
    echo "Illegal number of parameters"
    exit
fi
if [ $1 == "." ]; then
	home_loc=`pwd`
else
	home_loc=$1
fi
cd $home_loc
python3.6 -m compileall .
cd $home_loc/__pycache__
dtstr=`date +"%s"`
foldr=$dtstr
mkdir $foldr
for f in *.pyc; do 
    cp ./$f  "./"$foldr/"$(awk -F '[.]' '{print $1"."$3 }' <<<"$f")"
done
cd $home_loc
cp -r static ./__pycache__/$foldr
cp -r templates ./__pycache__/$foldr
cd  $home_loc/__pycache__/$foldr
zip -r $foldr.zip *
mv $foldr.zip  $home_loc
cd $home_loc
\rm -rf  $home_loc/__pycache__/$foldr
\rm -rf  $home_loc/__pycache__

