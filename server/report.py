'''
@author Divyansh Chopra

The file expects the data to be in the form of a dict which contains tuples.
For example - {"headers":(h1,h2,h3...), "1":(v1,v2,v3...), "2":(v1,v2,v3...), ...}
Here Headers tuple will have the headings that have to be placed in the xls sheet
'''
from openpyxl import Workbook
class Reporter:
	'''
	The main class to create reports.
	'''
	def __init__(self, password=None):
		self.wb = Workbook()
		self.ws = self.wb.active
		self.ws.title = 'Report'
		self.ws.protection.sheet = True
		if password is not None:
			self.wb.security.workbookPassword = str(password)
		self.wb.security.lockStructure = True

	def generate_report(self, data, name):
		'''
		 The data over here should be of the format
		:param data: {"headers":(h1,h2,h3...), "1":(v1,v2,v3...), "2":(v1,v2,v3...), ...}
		:param name: The name that should be given to the excel workbook.
		'''
		#
		#
		self.ws.append(data['headers'])
		data.pop('headers')
		for key in sorted(data):
			self.ws.append(data[key])
		self.ws.protection.enable()
		self.wb.save(str(name)+'.xlsx')

if __name__ == "__main__" :
	r = Reporter()
	r.generate_report(None,'sample')