from common import *
from shutil import copyfile
import mylog
import sys,os,time,random
from pymongo import MongoClient, DESCENDING
import user
import s3wrapper
import dbwrapper
from zipfile import ZipFile
import storewrapper
from bson.objectid import ObjectId


class Downloader:

    def __init__(self):
        try:
            self.srvr_db = dbwrapper.ServerDB()
            self.db = self.srvr_db.get_db()
            self.user = user.User(self.db)
            self.s3 = s3wrapper.My_S3()
            self.storagelocations = {}
            #self.storageinitialized = False
            #self.download_cache = storewrapper.Folder(get_download_cache_path())
            self.source = {}
            self.log = mylog.log
        except IOError as e:
            mylog.log.error("I/O error({0}): {1}".format(e.errno, e.strerror))
        except ValueError:
            mylog.log.error("Could not convert data to an integer.")
        except:
            mylog.log.error("Unexpected error:", sys.exc_info()[0])
            raise

    # TODO: Add size of files and avergae download speed from current default
    # storage to decide whether the given folder or file can be downloaded
    # syncronously or not. For place holding 10 files is the limit

    def get_storage_locations(self):
        #Get available storage options so that downloader can decide which one to select
        #If a chunk is not available in first location, it would be tried from second location
        #The list is prepared based on "priority for download" defined in the configuration of storage locations
        #The function intend to only initiate objects of all available storages
        allstorages = self.srvr_db.get_storage_list()
        if self.storagelocations:
            # we have already initiated.
            return self.storagelocations
        self.storagelocations = []
        self.storagelocations.append(storewrapper.Folder(get_save_to_path()))
        for aStorage in allstorages:
            id = aStorage["_id"]
            stype = aStorage["settings"]["type"]
            if stype == 'disk':
                try:
                    fldr = aStorage["settings"]["folder"]
                    sobj = storewrapper.Folder(fldr)
                    self.storagelocations.append(sobj)
                except:
                    pass #self.storagelocations.append(
            if stype == 'aws':
                try:
                    sobj = storewrapper.AWS_storage(aStorage["settings"])
                    self.storagelocations.append(sobj)
                except:
                    pass #self.storagelocations.append(None)
        return self.storagelocations

    def can_download_quickly(self, filename, devid):
        #if filename is like version, see the size and thats it. If not,
        # find the latest version of the file and see the size
        # if its a path, see the number of files and then total size for each
        # of them and then decide.
        if filename.startswith("/download/version/") :
            vid = filename.split("/download/version/")[1]
            result = self.db.files.find_one({"_id":ObjectId(vid)}, {"size":1})
            if result["size"] <= min_chunking_limit:
                return list(result)
            else:
                return False
        if filename.startswith("/download"):
            filename = filename.split('/download', 1)[1]
            parent, filename = filename.rsplit('/', 1)
        result = self.db.files.find({'path':'/'+filename, "parent":parent, "devid":devid})
        if result:
            # Just downloading a version
            if len(list(result)) ==1:
                return list(result)
        result = self.srvr_db.get_all_files(filename, devid)
        if result and (len(result) < 10):
            # Small data..just less than 10 files in total
            return result
        else:
            # Big folder
            return False

    def put_to_download_queue(self, files, devid, uid):
        id = self.db.downloadqueue.insert_one({"path":str(files), "devid":int(devid), "uid":int(uid), "done":int(0), "status":"notStarted", "tmpFolder":""})
        return (0,id)

    def check_download_status(self, id):
        result = self.db.downloadqueue.find_one({"_id":id})
        return  result["done"] == result["total"]

    # Downloads just a file. Do not expect full folder download with it.
    def download_a_file(self, filename, devid,  destFolder):
        storages = self.get_storage_locations()
        if filename.startswith("/version/"):
            version = filename.split("/version/")[1]
            fileinfo = self.db.files.find_one({"_id":ObjectId(version)})
            hashes = []
            for aHash in fileinfo['hsh']:
                result2 = self.db.hashtable.find_one({"idx": aHash})
                hsh = result2['_id']
                hashes.append(hsh)
                #find out the preferred source location first.
                for aStore in storages:
                    try:
                        aStore.get_from_store(hsh, destFolder)
                        break
                    except:
                        continue
            filename = fileinfo['path']
        else:
            #Get the latest version
            if filename.startswith("/download/"):
                filename = filename.split('/download', 1)[1]
            if '\\' in filename:
                filename = filename.replace('\\', '/')
            parent, filen = filename.rsplit('/', 1)
            filen = '/'+filen
            hashes = []
            result = self.db.files.find({"parent":parent, "path":filen, "devid":devid}).sort([("version", DESCENDING)])
            for res in result:
                #collect all hashes and then create the original file
                for aHash in res['hsh']:
                    result2 = self.db.hashtable.find_one({"idx": aHash})
                    hsh = result2['_id']
                    hashes.append(hsh)
                    for aStore in self.get_storage_locations():
                        try:
                            aStore.get_from_store(hsh, destFolder)
                            break
                        except:
                            continue
                break # a forced way to iterate only once. result[0] did not work and also find_one do not have sort.
        #Join chunks to make it file
        if filename.startswith('/'):
            filename = filename.rsplit('/', 1)[1]
        print("Trying to create file:", filename)
        f = open(os.path.join(destFolder, filename), "wb")
        for hsh in hashes:
            with open(os.path.join(destFolder,hsh), 'rb') as h:
                f.write(h.read())
            os.remove(os.path.join(destFolder, hsh))
        f.close()

        return filename

    # Writing rest functionality as function here. Will call in thread after this
    def download_a_folder(self, folder, devid, tmpFolder=None, id=None ):
        if not tmpFolder or (tmpFolder == ""):
            tmpFolder = os.path.join(get_download_cache_path(), str(int(time.time())) + str(random.randint(100, 999)))
            os.makedirs(tmpFolder, exist_ok=True)
            if id is not None:
                self.db.downloadqueue.update({"_id": id}, {'$set': {'tmpFolder': tmpFolder}})

        filelist = self.srvr_db.get_all_files(folder, devid)
        if id is not None: #Means its a processing of download queue
            self.db.downloadqueue.update({"_id":id}, {'$set': {'count': len(filelist)}})
        for oneFile in filelist:
            real_file_name = self.download_a_file(oneFile[0]+oneFile[1], devid, tmpFolder)
            if id is not None:  # Means its a processing of download queue
                self.db.downloadqueue.update({"_id": id}, {'$inc': {'done': 1}})
        return tmpFolder

    #
    def zip_the_folder(self, tmpFolder, zip_name):
        files_done = []
        with ZipFile(zip_name, 'w') as myzip:
            for root, dirs, files in os.walk(tmpFolder, topdown=True):
                for name in files:
                    afile = os.path.join(root, name)
                    if afile != zip_name:
                        print(afile)
                        myzip.write(afile, arcname=name)
                        files_done.append(afile)
                for name in dirs:
                    print(os.path.join(root, name))
        for afile in files_done:
            if afile != zip_name:
                os.remove(afile)

    def set_default_download_source(self, src_type="disk", src_path=None):
        if src_path is None:
            src_path = config.get('TEMP', 'upload_temp_location')
        self.source['type'] = src_type
        self.source['path'] = src_path

    def get_list_downloads(self, uid):
        result = self.db.downloadqueue.find({"uid":uid})
        listing = []
        for aDownload in result:
            if aDownload["status"] == "finished":
                zippath = os.path.basename(aDownload['download_path'])
                listing.append((aDownload["path"], aDownload["status"], aDownload["count"], aDownload["done"],zippath ))
            else:
                count = aDownload.get("count", 0)
                done = aDownload.get("done", 0)
                listing.append((aDownload["path"], aDownload["status"], count, done, None))
        return listing

    def remove(self, zipname):
        import re
        regx = re.compile(zipname+"$", re.IGNORECASE)
        try:
            zipfolder = zipname.split('.')[0]
            toDelete = os.path.join(self.source["path"],"temp", zipfolder,zipname)
            os.remove(toDelete)
            os.rmdir(os.path.join(self.source["path"],"temp", zipfolder))
            self.db.downloadqueue.delete_one({"download_path":regx})
            return True
        except:
            return False

    def process_download_queue(self):
        # If start of process (server reboot??), mark all downloads in queue as not-started
        # Get the item to download in loop
        # create a thread and handover to it
        # if temp folder is not defined in db (previous half done), create a new temp folder
        # add temp folder location to downloadqueue
        # get file list to download - stored location, actual filename, actual path
        # download a file
        # increase downloaded count in downloadqueue db
        # zip when done
        # mark status as OK
        global downloader_start
        try:
            if downloader_start: pass
        except:
            #this is first time
            downloader_start = True
            download_queue = self.db.downloadqueue.find({"status": {"$ne": 'finished'}})
            for dq in download_queue:
                self.db.downloadqueue.update({"_id":dq['_id']},{'$set':{'status':'notStarted'}})



        # Lets run the loop and TODO: Separate Threads per download request
        download_queue = self.db.downloadqueue.find({"status":'notStarted'})
        for dq in download_queue:
            dq_tmp = self.download_a_folder(dq['path'], dq['devid'], None, dq['_id'])
            dq_tmp = os.path.abspath(dq_tmp)
            folderName = os.path.basename(dq_tmp)
            zip_name = os.path.join(dq_tmp, folderName)+'.zip'
            self.zip_the_folder(dq_tmp, zip_name)
            self.db.downloadqueue.update({'_id':dq['_id']}, {'$set':{'download_path':zip_name, 'status':'finished'}})
