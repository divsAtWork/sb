import os, sys
import time

LOG_ALL = 0
DEBUGL = 1
ERRORL = 2
INFOL = 3

class Log:
    def __init__(self, level=LOG_ALL):
        #if self.lp is None:
        #    self.lp = None
        self.level = level
        self.console = None

    def set_console(self, op):
        self.console = op

    def start(self):
        try:
            self.lp
            return
        except:
            logdir = os.path.dirname(self.logfilename)
            os.makedirs(logdir, exist_ok=True)
            self.lp = open(self.logfilename, "a+")

    def print_log(self, level, *log):
        DEBUG = False
        if "debug" in sys.argv:
            DEBUG = True
        if DEBUG:
            print(log)
        if self.lp and level>= self.level:
            self.lp.write(time.strftime('%x %X'))
            self.lp.write(" : ")
            for item in log:
                self.lp.write(str(item))
            self.lp.write('\n')
            self.flush()
            return log
        else:
            return ''

    def info(self, *log):
        return self.print_log(INFOL, *log)

    def debug(self, *log):
        return self.print_log(DEBUGL, *log)

    def error(self, *log):
        return self.print_log(ERRORL, *log)

    def all(self, *log):
        return self.print_log(LOG_ALL, *log)

    def close(self):
        return self.lp.close()

    def set_log_level(self, new_level):
        self.level = new_level

    def set_log_file_name(self, logfilename):
        self.logfilename = logfilename

    def write(self, msg):
        self.lp.write(msg)
        if self.console:
            self.console.write(msg)
        self.flush()

    def flush(self):
        self.lp.flush()

    def flushall(self):
        self.lp.flush()
####

global log
import os
logpath = os.path.join('..', 'log')
os.makedirs(logpath, exist_ok=True)
log = Log(os.path.join(logpath, 'server.log'))