import sys, time
from common import *
import json
import re


class User:
    def __init__(self, db):
        self.db = db

    def get_user_id(self, session_obj):
        return session_obj['uid']

    def get_licenses(self):
        result = self.db.user.find_one({"_id":int(0)})
        if result:
            return int(json.loads(decr_data(result['key']))['count'])

    def get_users_list(self, type_of_user='all', search=None):
        if search:
            usrregx = re.compile(search, re.IGNORECASE)
            result = self.db.user.find({"type": 'user', '$or':[{'fname':usrregx},{'lname':usrregx},{'name':usrregx},{'email':usrregx}]})
        else:
            result = self.db.user.find({"type": 'user'}, {'shortcuts': 0})
        retval = []
        for res in result:
            state = res.get('state', 1)
            # Hide built-in Admin
            if int(res['_id']) != 0:
                total_usage = 0
                devs = self.db.devices.find({'uid': int(res['_id'])})
                if devs is not None:
                    for one in devs:
                        total_usage += int(one['size'])
                usage = size_str(total_usage, 2)
                retval.append({'id': res['_id'], 'name': str(res['fname'] + ' ' + res['lname']), 'type': res['type'],
                               'isAllowed': True, 'activated': state, 'quota': size_str(res['quota'], 2),
                               'usage': usage})
        return retval

    def get_user_details(self, uid):
        result = self.db.user.find_one({'_id': uid})
        username = result['name']
        shortcuts = result['shortcuts']
        quota = result.get('quota', 0)
        fname = result.get('fname', 'No First Name specified')
        lname = result.get('lname', 'No Last Name specified')
        devices = {}
        result = self.db.devices.find({'uid': uid})
        total_usage = 0
        if result is not None:
            for one in result:
                devices[str(int(one['devid']))] = {'name': one['name'], 'size': size_str(one['size'], 2)}
                total_usage += int(one['size'])
        return {'id': uid, 'name': username, 'shortcuts': shortcuts, 'devices': devices, 'usage': total_usage,
                'quota': quota, 'fname': fname, 'lname': lname}

    def get_next_user_id(self):
        try:
            x = self.db.user_idx.find({}, {'_id': 1, 'count': 1})
            if x == None:
                self.db.user_idx.insert_one({'count': 1})
                return 1
            else:
                cnt = 0
                iDD = '1'
                for y in x:
                    iDD = y['_id']
                    cnt = y['count'] + 1
                    result = self.db.user.find_one({'_id': 0})
                    import common as kom
                    k = kom.key2cnt(result.get('key', ""))
                    if cnt ^ int(k):
                        self.db.user_idx.update({'_id': iDD}, {'count': cnt})
                        return cnt
                # Intentionally out of for loop as we need just entry
                # Also if db is blank, this entry must go in.
            self.db.user_idx.insert_one({'count': 1})
            return 1
        except:
            self.db.user_idx.insert_one({'count': 1})
            return 1

    def is_user_login(self, user, pwd_hash):
        result = self.db.user.find_one({'name': user, 'pwd': pwd_hash}, {'_id': 1, 'type': 1})
        if result != None:
            return result['_id'], result['type']
        else:
            return None, 'None'

    def add_user(self, user, pwd_hash, state, utype=USER, lname='', fname='', quota=0, shortcuts={'home': '/'},
                 ldap_details=None):
        new_id = self.get_next_user_id()
        def insert_into_group_ALL():
            try:
                document = dict(self.db.groups.find_one({"gid": 0}))
                document['members'].append(new_id)
                self.db.groups.update({"gid": 0}, document)
            except:
                self.db.groups.insert_one({"gid": int(0), "name": "ALL", "mode": "internal", "members": [new_id, ]})
            return
        if ldap_details is None:
            if new_id:
                result = self.db.user.insert_one(
                    {'_id': new_id, 'name': user, 'pwd': pwd_hash, 'state': state, 'lname': lname, 'fname': fname,
                     'email': user, 'type': utype, 'shortcuts': shortcuts, 'quota': quota})
                if result is not None:
                    insert_into_group_ALL()
                    return new_id
                else:
                    return -1
        else:
            # TODO: Do we add another field like createdBy and ldap_setting_name?
            if new_id:
                result = self.db.user.insert_one({'_id': new_id, 'name': ldap_details['name'], 'pwd':'lena hai abhi',
                                                  'state': 0, 'email': ldap_details['email'], 'type': utype,
                                                  'shortcuts': shortcuts, 'quota': quota, 'distinguishedName': ldap_details['distinguishedName'],
                                                  'sAMAccountName': ldap_details['sAMAccountName']})
                if result is not None:
                    insert_into_group_ALL()
                    return new_id
                else:
                    return -1

    def user_exists(self, name):
        result = self.db.user.find_one({'name': name})
        if result is not None:
            return True
        else:
            return False

    def save_user(self, name):
        query = {"name": name}
        self.db.user.insert_one(query)

    def set_user_state(self, id, action):
        action_array = ['pending', 'allow', 'deny', 'revoke', 'delete']
        if action not in action_array:
            return None
        action = action_array.index(action)
        if action != 4:  # index of 'delete'
            return self.db.user.update({'_id': int(id)}, {'$set': {'state': action}})
        else:
            return self.db.user.remove({'_id': int(id)})

    def verify_device_by_key(self, uid, devid, device_key):
        result = self.db.devices.find_one({"key": device_key}, {"uid": 1, "devid": 1})
        if result != None:
            if int(uid) == int(result['uid']) and int(devid) == int(result['devid']):
                return True
        return False

    def updateUser(self, web_input, pwd_hash=''):
        uid = int(web_input['user_id'])
        result = self.db.user.find_one({'_id': uid})
        if str(web_input['fname']) != str(result['fname']):
            self.db.user.update({'_id': uid}, {'$set': {'fname': str(web_input['fname'])}})
        if str(web_input['lname']) != str(result['lname']):
            self.db.user.update({'_id': uid}, {'$set': {'lname': str(web_input['lname'])}})
        if str(web_input['quota']) != str(result['quota']):
            self.db.user.update({'_id': uid}, {'$set': {'quota': int(str(web_input['quota']))}})
        if str(web_input['name']) != str(result['name']):
            self.db.user.update({'_id': uid}, {'$set': {'name': web_input['name']}})
        if pwd_hash != '':
            self.db.user.update({'_id': uid}, {'$set': {'pwd': pwd_hash}})
