from common import *
import dbwrapper

class Reports:
	def __init__(self):
		self.conn = dbwrapper.ServerDB()
		self.db = self.conn.db.reports


	def get_list(self):
		try:
			result = list(self.db.find())
		except:
			return [{}]
		return result

	def add_report(self, param):
		return self.db.insert(param)