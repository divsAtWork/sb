var allow_btns = document.getElementsByClassName("btn btn-warning btn-sm allow");
var i;
for(i=0;i<allow_btns.length;i++)
{
  allow_btns[i].onclick = function(){
    var item = $(this).closest("tr").text();
    var list = item.split('\n');
    var modal_body = document.getElementById('allow-user');
    modal_body.value = list[1].trim();
  }
}

//var deny_btns = document.getElementsByClassName("btn btn-danger btn-sm deny");
//for(i=0;i<deny_btns.length;i++)
//{
//  deny_btns[i].onclick = function(){
//    var item = $(this).closest("tr").text();
//    var list = item.split('\n');
//    var modal_body = document.getElementById('deny-user');
//    modal_body.value = list[1].trim();
//  }
//}

function deny_user(uid, act){
    var modal_body = document.getElementById('deny-user');
    document.getElementById("uid").value=uid;
    document.getElementById("act").value=act;
    document.getElementById("modal_act").innerHTML='Are you sure you want to ' +act+ ' this user?';


}

function send_action(){
    document.getElementById("user-action-form").submit();
}

function get_input_tag(label, name){
  var myvar = '<div class="form-group">'+
                '<label for="user-fname" class="col-form-label">'+label+':</label>'+
                '<input type="text" class="form-control" id="'+name+'" name="'+name+'">'+
                '</div>';
  return myvar;
}

function get_input_list(label, usr_list) {
    var retVal = "<div class='form-group'>"+
                "<label for='user_list_select' class='col-form-label'>"+label+": </label>"+
                "<select name='user_list_select' id='user_list_select'><option value='select_one'>Select</option>"
    for(i=0;i<usr_list.length;i++){
        if(usr_list[i].name != ""){
            retVal += "<option value="+usr_list[i].id+">"+usr_list[i].name+"</option>"
        }
    }
    retVal += "</select></div>"
    return retVal
};

function storage_options() {
  var x = document.getElementById("storage_type").value;
  if(x == "disk"){
    document.getElementById("store_options_id").innerHTML= get_input_tag('Location', 'param')
  }
  if(x == 'aws'){
    var z = get_input_tag('Key', 'key')+get_input_tag('Secret','secret');
    document.getElementById("store_options_id").innerHTML = z;
  }
}