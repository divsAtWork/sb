import os,sys,traceback
import web
import mylog
import dbwrapper
from common import *
import common as cmn
from shutil import copyfile

render = web.template.render('templates/', base="layout")
log = mylog.log
session = cmn.session

#global session
def answer(page, **kwargs):
    browser = web.ctx.env.get('HTTP_USER_AGENT', "API")
    try:
        if browser not in ('API', 'python-requests/2.18.4'):
            if (page == web.seeother) or (page==web.redirect):
                page(kwargs['redirect_to'])
            else:
                return page(kwargs)
        else:
            #API specific returns
            web.config.debug=False
            return kwargs
    except:
        traceback.print_exc()
        return "Something Went wrong"

def isAdmin():
    if cmn.session.get('utype', 'user')== 'admin':
        return True
    else:
        return False

def validate_admin(someone, *args):
    def wrapper(*args):
        try:
            print("Session Count = ", cmn.session['count'])
            if cmn.session['count'] >= 1:
                if isAdmin():
                    return someone(*args)
            else:
                return answer(web.seeother, redirect_to='/index')
        except IOError as e:
            log.error("I/O error({0}): {1}".format(e.errno, e.strerror))
        except ValueError:
            log.error("Could not convert data to an integer.")
        except:
            log.error("Unexpected error:", sys.exc_info()[0])
        return answer(web.seeother, redirect_to='/index')
    return wrapper

class Folder:
    # A class for giving pointer to a local disk folder and have operations like store etc from it.
    def __init__(self, folder_name):
        self.foldername = folder_name

    def get_save_to_path(self):
        return config.get('TEMP', 'upload_temp_location')


    #Save the data (hash as file name) to the system.
    def save(self, hsh, source=None):
        if source is None:
            source = os.path.join(self.get_save_to_path(), hsh[0],hsh[1],hsh[2],hsh[3],hsh[4], hsh)
        destination_folder = os.path.join(self.foldername,hsh[0],hsh[1],hsh[2],hsh[3],hsh[4])
        os.makedirs(destination_folder, exist_ok=True)
        print("Copying ",hsh," to ", os.path.join(destination_folder,hsh))
        try:
            copyfile(source, os.path.join(destination_folder,hsh))
            return True
        except:
            print("Error in saving chunk ", hsh)
            return False

    #Get a hash
    def get_from_store(self, hsh, destFolder):
        source = os.path.join(self.foldername,hsh[0],hsh[1],hsh[2],hsh[3],hsh[4],hsh)
        #This function will be called for this folder only..
        #Decision to whether use this folder or not should be done
        #on a lyer above (from where this function is clalled
        destination = os.path.join(destFolder,hsh)
        if os.path.isfile(source):
            copyfile(source, destination)
            return destination
        else:
            raise Exception('Source do not have this file')

    def remove_from_store(self, hsh):
        source = os.path.join(self.foldername,hsh[0],hsh[1],hsh[2],hsh[3],hsh[4],hsh)
        #This function will be called for this folder only..
        #Decision to whether use this folder or not should be done
        #on a lyer above (from where this function is clalled
        if os.path.isfile(source):
            try:
                os.remove(source)
            except:
                pass
        return source

class AWS_storage:
    def __init__(self):
        return True

    def get_config(self):
        return True

    def validate_connection(self):
        return  True

    def save(self, hsh):
        return  True

    def get_from_store(self, hsh, destFolder):
        return True

#===============================
#
#  UI FUNCTIONS
#
#===============================
class storage:

    def __init__(self):
        self.db = dbwrapper.ServerDB()

    #Function to show Storage list to the admin
    @validate_admin
    def GET(self):
        storage_list = self.db.get_storage_list()
        if 'migrate' in web.ctx.fullpath:
            return answer(render.migrateStorage, storage=storage_list, msg="")
        else:
            return answer(render.storage, storage=storage_list, msg="")

    # Function to manage storage lists, their status from Admin console
    @validate_admin
    def POST(self):
        i = web.input()
        storage_list = self.db.get_storage_list()
        if 'migrate' in web.ctx.path:
            return answer(render.migrateStorage, storage=storage_list, msg="Migration Successful") if self.db.initiate_storage_migration(i) else answer(render.migrateStorage, storage=storage_list, msg="Error while migrating")
        option = i.storage_options
        if option in (STORAGE_ACTIVE, STORAGE_INACTIVE, STORAGE_REMOVED):
            id = i.iD
            try:
                if self.db.set_storage_state(id, option):
                    storage_list = self.db.get_storage_list()
                    return answer(render.storage, storage=storage_list, msg="Activated")
                raise
            except:
                i.param = ""

        print("Opted to have a new storage of %s type" % option)
        print(i.param)
        if i.param == "":
            storage_list = self.db.get_storage_list()
            return answer(render.storage, storage = storage_list, msg="    Blank Path provided. Provide path properly")
        params = {}
        params["type"] = option
        if option == 'disk':
            params['folder'] = i.param
        if option == 'aws':
            params['bucket_name'] = i.bucket_name
            params['key']   = i.param
            params['secret']= i.secret
        if option == 'cifs':
            params['server'] = i.param
            params['port'] = i.port
            params['sharename'] = i.sharename
            params['username'] = i.username
            params['password'] = i.passwd
            params['mount_location'] = i.mnt
        if option == 'nfs':
            params['server'] = i.param
            params['port'] = i.port
            params['sharename'] = i.sharename
            params['username'] = i.username
            params['password'] = i.passwd
        params['size'] = get_byte_from_value_str(i.size_of_storage)
        try:
            self.db.create_storage(params)
        except:
            storage_list = self.db.get_storage_list()
            return answer(render.storage, storage = storage_list, msg="Improper values provided")
        #get the latest list again
        storage_list = self.db.get_storage_list()
        return answer(render.storage, storage = storage_list, msg="Added Successfully")

class storagelist:
    def __init__(self):
        self.db = dbwrapper.ServerDB()

    @validate_admin
    def POST(self):
        import json
        x = json.loads(web.data())
        y = self.db.get_storage_details(int(str(x['id'])))
        y['settings']['size'] = size_str(y['settings']['size'], 2)
        return json.dumps(y)

class storageUpdater:
    def __init__(self):
        self.db = dbwrapper.ServerDB()

    @validate_admin
    def POST(self):
        i = web.input()
        result = self.db.get_storage_details(int(str(i['id'])))
        if str(i['type']) == 'disk':
            if i.eparam == "":
                storage_list = self.db.get_storage_list()
                return answer(render.storage, storage=storage_list, msg="    Blank Path provided. Provide path properly")
            if i.eparam != result['settings']['folder']:
                self.db.storage.update({"_id": int(str(i['id']))}, { '$set': {'settings.folder' : str(i.eparam)} })
        elif str(i['type']) == 'aws':
            if i.eparam == "":
                storage_list = self.db.get_storage_list()
                return answer(render.storage, storage=storage_list, msg="    Blank Key provided. Provide a Key")
            if i.eparam != result['settings']['key']:
                self.db.storage.update({"_id": int(str(i['id']))}, {'$set': {'settings.key': str(i.eparam)}})
            if i.ebucket_name != result['settings']['bucket_name']:
                self.db.storage.update({"_id": int(str(i['id']))}, {'$set': {'settings.bucket_name': str(i.ebucket_name)}})
            if i.esecret != result['settings']['secret']:
                self.db.storage.update({"_id": int(str(i['id']))}, {'$set': {'settings.secret': str(i.esecret)}})
        elif str(i['type']) == 'cifs':
            if i.eparam == "":
                storage_list = self.db.get_storage_list()
                return answer(render.storage, storage=storage_list, msg="    Blank Server address provided. Provide a valid address")
            if i.eparam != result['settings']['server']:
                self.db.storage.update({"_id": int(str(i['id']))}, {'$set': {'settings.server': str(i.eparam)}})
            if i.eport != result['settings']['port']:
                self.db.storage.update({"_id": int(str(i['id']))}, {'$set': {'settings.port': str(i.eport)}})
            if i.eusername != result['settings']['username']:
                self.db.storage.update({"_id": int(str(i['id']))}, {'$set': {'settings.username': str(i.eport)}})
            if i.epasswd != result['settings']['password']:
                self.db.storage.update({"_id": int(str(i['id']))}, {'$set': {'settings.password': str(i.epasswd)}})
            if i.emnt != result['settings']['mount_location']:
                self.db.storage.update({"_id": int(str(i['id']))}, {'$set': {'settings.mount_location': str(i.emnt)}})
        elif str(i['type']) == 'nfs':
            if i.eparam == "":
                storage_list = self.db.get_storage_list()
                return answer(render.storage, storage=storage_list, msg="    Blank Server address provided. Provide a valid address")
            if i.eparam != result['settings']['server']:
                self.db.storage.update({"_id": int(str(i['id']))}, {'$set': {'settings.server': str(i.eparam)}})
            if i.eport != result['settings']['port']:
                self.db.storage.update({"_id": int(str(i['id']))}, {'$set': {'settings.port': str(i.eport)}})
            if i.eusername != result['settings']['username']:
                self.db.storage.update({"_id": int(str(i['id']))}, {'$set': {'settings.username': str(i.eport)}})
            if i.epasswd != result['settings']['password']:
                self.db.storage.update({"_id": int(str(i['id']))}, {'$set': {'settings.password': str(i.epasswd)}})

        if str(get_byte_from_value_str(str(i.esize_of_storage))) != str(result['settings']['size']):
            self.db.storage.update({"_id": int(str(i['id']))},
                                   {'$set': {'settings.size': get_byte_from_value_str(i.esize_of_storage)}})
        storage_list = self.db.get_storage_list()
        return answer(render.storage, storage=storage_list, msg="")
