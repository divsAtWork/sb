import web
import dbwrapper
import s3wrapper
import threading
import hashlib
from common import *
import time, copy
import mylog
import signal
import json
import user, sys,os
import random
import backup
import traceback
import devices
import configparser
import storewrapper
import ldapwrapper as ld
import common as cmn
import reports

urls = (
    '/', 'index',
    '/index', 'index',
    LOGIN_API, 'login',
    LOGOUT_API, 'logout',
    USER_LIST_API, 'userlist',
    FILELIST_API + '/(.*)', 'filelist',
    UPLOAD_API, 'upload',
    DOWNLOAD_API + '/(.*)', 'download',
    REGISTER_USER_API, 'register',
    REGISTER_CLIENT_API, 'regcli',
    USERHOME_API, 'userhome',
    ADMINHOME_API, 'adminhome',
    GET_POLICY_API, 'policy',
    ADMIN_STORAGE_API, 'storewrapper.storage',
    '/backup(.*)', 'backup_router',
    DEVICE_API, 'devices.Devices',
    '/migrateStorage', 'storewrapper.storage',
    SETTINGS_API, 'settings',
    '/chunkcheck', 'chunk',
    '/chunkput', 'chunk',
    '/chunkmeta', 'chunk',
    '/edit', 'edit',
    '/storageinfo', 'storewrapper.storagelist',
    '/editStorage', 'storewrapper.storageUpdater',
    '/ldap', 'ldap',
    GROUP_API, 'groups',
    '/basedn', 'base_dn',
    '/import', 'imports',
    '/infoBackup', 'backup_info',
    '/reports', 'reporting'
)

app = web.application(urls, globals())
# global session
if not web.config.get('session'):
    ### Define the initial session
    ### Store the data in the directory './sessions'
    init = {'count': 0, 'devid': 0, 'uid': 0}
    store = web.session.DiskStore('../../sessions')
    session = web.session.Session(app, store, initializer=init)
    ### Store it somewhere we can access
    web.config.session = session
    cmn.session = session
else:
    ### If it is already created, just use the old one
    session = web.config.session

# Initialize db and template pointers
srvr_db = dbwrapper.ServerDB()
myusers = user.User(srvr_db.get_db())
my_s3 = s3wrapper.My_S3()
render = web.template.render('templates/', base="layout")
login_render = web.template.render('templates/', base="login_layout")
client_render = web.template.render('templates/')

ldapwrapper = ld.LdapWrapper()

log = mylog.log
mylog.log = mylog.Log(int(config.get(LOG_CONFIG_KEY, LOG_LEVEL_CONFIG_KEY)))

import download as dnld
dwnload = dnld.Downloader()
dwnload.set_default_download_source()
dwnload.log = mylog.log


##########    G E N E R A L   F U N C T I O N S ########
def validate_login(someone):
    def wrapper():
        print("I am called")
        return someone()
    return wrapper

def validate_admin(someone, *args):
    def wrapper(*args):
        try:
            print("Session Count = ", session['count'])
            if session['count'] >= 1:
                if isAdmin():
                    return someone(*args)
            else:
                return answer(web.seeother, redirect_to='/index')
        except IOError as e:
            log.error("I/O error({0}): {1}".format(e.errno, e.strerror))
        except ValueError:
            log.error("Could not convert data to an integer.")
        except:
            traceback.print_exc()
            log.error("Unexpected error:", sys.exc_info()[0])
        return answer(web.seeother, redirect_to='/index')
    return wrapper

def validate_user(someone, *args):
    def wrapper(*args):
        try:
            print("Session Count = ", session['count'])
            if session['count'] >= 1:
                if session['utype'] in ('user', 'device'):
                    return someone(*args)
            else:
                return answer(web.seeother, redirect_to='/index')
        except IOError as e:
                log.error("I/O error({0}): {1}".format(e.errno, e.strerror))
        except ValueError:
            log.error("Could not convert data to an integer.")
        except:
            traceback.print_exc()
            log.error("Unexpected error:", sys.exc_info()[0])
        return answer(web.seeother, redirect_to='/index')
    return wrapper

def validate_user_or_admin(someone, *args):
    def wrapper(*args):
        try:
            print("Session Count = ", session['count'])
            if session['count'] >= 1:
                    return someone(*args)
            else:
                return answer(web.seeother, redirect_to='/index')
        except IOError as e:
                log.error("I/O error({0}): {1}".format(e.errno, e.strerror))
        except ValueError:
            log.error("Could not convert data to an integer.")
        except:
            traceback.print_exc()
            log.error("Unexpected error:", sys.exc_info()[0])
        return answer(web.seeother, redirect_to='/index')
    return wrapper

def answer(page, **kwargs):
    browser = web.ctx.env.get('HTTP_USER_AGENT', "API")
    try:
        if browser not in ('API', 'python-requests/2.18.4'):
            if (page == web.seeother) or (page==web.redirect):
                page(kwargs['redirect_to'])
            else:
                return page(kwargs)
        else:
            #API specific returns
            web.config.debug=False
            return kwargs
    except:
        traceback.print_exc()
        return "Something Went wrong"

def isAdmin():
    if session.get('utype', 'user')== 'admin':
        return True
    else:
        return False

def generate_temporary_filename():
    return str(time.time())+str(random.randrange(111111,999999))


########     R O U T E R  and  W O R K E R    C L A S S E S   #########
class index:
    def GET(self, eror=None):
        #i = web.input(name={}, password={})
        try:
            session['count'] = session['count']
        except:
            session['count'] = 0
        #srvr_db.save_user(i.name)
        log.debug("At index page with session count as ",session['count'])
        return login_render.index(eror=eror)

class login:
    global srvr_db
    def GET(self):
        return index.GET(self)

    def POST(self):
        i = web.input()
        try:
            #i = json.loads(i)
            key = i.key
            keys = str(key)
            data_str = decr_data(keys)
            data = json.loads(data_str.replace("'", '"'))
            uid = int(data['uid'])
            devid = int(data['devid'])
            device_key = data['key']
            if myusers.verify_device_by_key(int(uid), int(devid), device_key):
                #Login Successful
                web.session.username = myusers.get_user_details(uid)['name']
                session['count'] = 1  # Means logged in
                session['uid'] = uid
                session['utype'] = 'device'
                session['devid'] = devid
                log.info("Successful login from device of "+web.session.username)
                user_details =  myusers.get_user_details(uid)
                session['shortcuts'] = user_details['shortcuts']
                return user_details
            else:
                log.info("Attack possible - Someone trying to login with invalid key")
                return "LOGIN_FAIL"
        except:
            #Normal user or Admin login via GUI
            pass
        browser = web.ctx.env.get('HTTP_USER_AGENT', "API")
        uname = i.name
        pwd1 = i.password
        pwd1 = pwd1.encode('utf-8')
        pwdhash = hashlib.md5(pwd1).hexdigest()
        #Validate credentials
        log.info("Login attempted for ",uname)
        uid,utype = myusers.is_user_login(uname, pwdhash)

        if uid!=None:
            web.session.username = i.name
            session['count'] = 1 #Means logged in
            session['uid'] = uid
            session['utype'] = utype
            if utype == 'user':
                try:
                    #use the one provided by agent
                    devid = int(i.devid)
                except:
                    #get online device id for this user
                    devid = srvr_db.get_device_id(session)
                session['devid'] = devid
            else:
                #No device for admin
                pass

            if utype == USER or utype == 'device':
                retval = myusers.get_user_details(uid)
                session['shortcuts'] = retval['shortcuts']
                log.info("Login successful with uid=",uid)
                return answer(web.redirect, redirect_to='/userhome', args=retval)
            else:
                raise web.redirect('/adminhome')
        else:
            session['count'] = 0
            return login_render.index(eror="Incorrect details")

class logout:
    def GET(self):
        session.kill()
        return login_render.index(eror=None)

class userlist:
    @validate_admin
    def GET(self):
        # return answer(web.redirect, redirect_to='adminhome')
        user_id = web.input(uid = None)
        if user_id is None:
            return answer(web.redirect, redirect_to='adminhome')
        else:
            return myusers.get_user_details(user_id)
        # return answer(render.userlist, len=len(users), users=users)

    @validate_admin
    def POST(self):
        #Set user state
        x = web.data()
        import json
        x = json.loads(x)
        id = x['uid']
        if int != type(id) or id == 0:
            return answer(web.redirect, redirect_to='adminhome')
        return json.dumps(myusers.get_user_details(id))

class filelist:
    @validate_user
    def GET(self, prefix):
        if prefix==None: prefix="/"
        if 'DEVICE_ROOT' in prefix:
            aaa = prefix.split('DEVICE_ROOT_')
            abc = aaa[1]
            devid = int(abc)
            session.devid = devid
            prefix = '/'
        else:
            devid = session.devid
        files, versions = srvr_db.get_file_list(prefix, devid)
        l = len(files)
        userdata = myusers.get_user_details(session.uid)
        # return answer(render.filelist, len=l, prefix=prefix, files=files, shortcuts=userdata['shortcuts'])
        return client_render.client_layout(client_render.filelist(parent=prefix, files=files), username=userdata['name'], shortcuts=userdata['shortcuts'])
        #print("Got retval", retval)
        #return retval
    def POST(self):
        i = web.input()
        print("Received action to update shortcuts")
        cur_loc = i['pwd']
        return answer(web.redirect, redirect_to='filelist/'+cur_loc)

class download:
    global srvr_db
    def GET(self, f):
        if f == "list":
            list_of_downloads = dwnload.get_list_downloads(session.uid)
            userdata = myusers.get_user_details(session.uid)
            return client_render.client_layout(client_render.downloadList(names_and_status=list_of_downloads),
                                               username=userdata['name'], shortcuts=userdata['shortcuts'])
            #return answer(render.downloadList, names_and_status=list_of_downloads)
        if 'cached/' in f:
            #zip file for download
            zipname = f.rsplit('/', 1)[1]
            zipfolder = zipname.split('.')[0]
            print("Send back ",zipname)
            web.header('Content-Disposition', 'attachment; filename="' + zipname + '"')
            web.header('Content-type', 'application/zip')
            web.header('Content-transfer-encoding', 'binary')
            fp = open(os.path.join(get_save_to_path(), zipfolder, zipname), 'rb')
            return fp.read()
        if 'remove/' in f:
            zipname = f.rsplit('/', 1)[1]
            zipfolder = zipname.split('.')[0]
            print("Removing ", zipname)
            dwnload.remove(zipname)
            return answer(web.seeother, redirect_to='/download/list')
        # Create a temporary folder for this particular download
        tmpStr =  str(int(time.time()))+str(random.randint(100, 999))
        tempPath = os.path.join(get_download_cache_path(),tmpStr)
        os.makedirs(tempPath, exist_ok=True)
        if f.startswith('/download'):
            f = f.split('/download', 1)[1]
        #files_to_download = dwnload.can_download_quickly(fileTmpPath, session.devid)
        #There are two possibilities
        # 1- Its just a file
        # 2 - Its a folder with few files.
        # IDea is to do synchronous download if files size and number of files is small.

        # Guess the path as Folder or File. If its folder, it must be parent of someone.
        result = srvr_db.get_db().files.find_one({"parent":f})
        if result:
            #OK. Its a folder
            dwnload.download_a_folder(f, session.devid, tempPath )
            zipname = os.path.join(tempPath,tmpStr+'.zip')
            dwnload.zip_the_folder(tempPath, zipname)
            web.header('Content-Disposition', 'attachment; filename="'+tmpStr+'.zip'+'"')
            #web.header('Content-type', 'application/pdf')
            web.header('Content-transfer-encoding', 'binary')
            fp=open(zipname, 'rb')
            return fp.read()
        else:
            # Its a file
            filename_to_report = dwnload.download_a_file(f, session.devid, tempPath)
            #will return here only after copy..so now return this file to the browser/agent
            web.header('Content-Disposition', 'attachment; filename="'+filename_to_report+'"')
            #web.header('Content-type', 'application/pdf')
            web.header('Content-transfer-encoding', 'binary')
            fp=open(os.path.join(tempPath, filename_to_report), 'rb')
            return fp.read()
        #No effect of lines below. Make them working later...
        dwnload.put_to_download_queue(f, session.devid, session.uid)
        return answer(web.seeother, redirect_to='/download/list')

class upload:
    global srvr_db

    @validate_user
    def GET(self):
        print("Upload for user", session.uid)
        userdata = myusers.get_user_details(session.uid)
        return client_render.client_layout(client_render.fileUpload(filepath=""), username=userdata['name'], shortcuts=userdata['shortcuts'])

    @validate_user
    def POST(self):
        x = web.input(files={})
        devid = srvr_db.get_device_id(session)
        uid = myusers.get_user_id(session)
        filedir = get_save_to_path() # change this to the directory you want to store the file in.
        if 'x.files'!= None:
            info = {}
            try:
                info = json.loads(x.info.decode('utf-8'))  # to check if the file-object is created
                fn1 = info['filename']
            except:
                fn1 = x.files.filename
            #fn2 = x.files.filename.decode('utf-8')
            #fn3 = str(fn2)
            dedup_chk = 0
            try:
                mtime = info['mtime']#'.decode('utf-8')
            except:
                #From browser so use server's time
                mtime = str(int(time.time()))
                #Check-for-dedup
                dedup_chk = 1
            db_file_name = copy.deepcopy(fn1)
            if '\\' in db_file_name:
                db_file_name=db_file_name.replace('\\','/') # replaces the windows-style slashes with linux ones.
                db_file_name=db_file_name.replace('//', '/')
                filename=db_file_name.split('/')[-1] # splits the and chooses the last part (the filename with extension)
                via_browser = False
            else:
                filename = db_file_name
                via_browser = True
            log.info("Received upload of ",db_file_name)
            rename_file = False
            try:
                hsh = tmpFileName =  x.hsh.decode('utf-8')
            except:
                tmpFileName = generate_temporary_filename()
                rename_file = True#get_file_hash(os.path.join(filedir,filename))
            fout = open(os.path.join(filedir,tmpFileName),'wb') # creates the file where the uploaded file should be stored
            fout.write(x.files.file.read()) # writes the uploaded file to the newly created file.
            fout.close() # closes the file, upload complete.
            if rename_file:
                hsh  = get_file_hash(os.path.join(filedir,tmpFileName))
                os.rename(os.path.join(filedir,tmpFileName), os.path.join(filedir,hsh))
                tmpFileName = hsh

            filesize = os.stat(os.path.join(filedir,tmpFileName)).st_size
            userdata = myusers.get_user_details(uid)
            if userdata['usage']+filesize > userdata['quota']:
                #Will exceed quota - so drop this file
                return answer(web.seeother, redirect_to='/upload', filename="QUOTA_FULL")
            #Now take hash of file

            old_file_size = srvr_db.is_duplicate(hsh, filesize)
            if old_file_size != None:
                dedup_chk = 2
            #Do all DB entries now
            if via_browser:
                actual_filename= "/online/"+db_file_name
            else:
                actual_filename = '/'+db_file_name
            hsh_id = []
            hsh_id.append(srvr_db.add_hash(hsh, filesize))
            srvr_db.query_and_create_path_entries(actual_filename, hsh_id, devid, mtime, filesize)
            idx = srvr_db.add_hash(hsh, filesize)
            srvr_db.queue_file_for_upload(idx, hsh)

            return answer(web.seeother, redirect_to='/upload', filename=filename)

class register:
    global srvr_db

    def register_device(self, param):
        print("Register This device")
        return "OK"

    def POST(self):
        print("Registration POST")
        i = web.input()
        try:
            utype = i.utype
        except:
            # user coming from self registration
            utype = USER

        #Case - Device registration
        if utype=='device':
            devid, retVal = srvr_db.register_new_device(i.uid, i.name)
            session['devid'] = devid
            return retVal

        #Case - User registration
        pwd1 = i.password.encode('utf-8')
        pwdhash = hashlib.md5(pwd1).hexdigest()
        error_msg = "Registered Successfully!!"
        if isAdmin():
            state = 1
        else:
            state = 0
            error_msg += "Ask admin to approve."
        try:
            quota=get_byte_from_value_str(i.quota)
        except:
            quota = 0

        if not myusers.user_exists(i.name):
            log.info("New user ",i.name," added")
            try:
                retval = myusers.add_user(i.name, pwdhash, state, utype , i.lname, i.fname, quota )
                if retval>0:
                    log.all("Added User", i.name, i.lname, i.fname)
                else:
                    reason = ["License exhausted", "User already exist", "Unable to connect to AD/LDAP", "Unknown Error"]
                    error_msg = reason[(0-retval)]
                    raise
            except:
                log.all("Error adding new User", i.name, i.lname, i.fname)
                error_msg = "Unknown error in adding user"
            if isAdmin():
                usrs = myusers.get_users_list()
                licens = myusers.get_licenses()
                return answer(render.adminhome, users=usrs, devices={}, licenses=int(licens), msg=error_msg)
            return index.GET(self, eror=error_msg)
            #return answer(web.seeother, redirect_to='/filelist/')
        else:
            return index.GET(self, eror="This User already exists!!")

class userhome:
    @validate_user
    def GET(self):
        userdata = myusers.get_user_details(session.uid)
        #devices = srvr_db.get_device_list_for_user(session.uid)
        bkplist = None
        devlist = userdata['devices']
        if userdata['devices'] and len(userdata['devices'])==1:
            print("Only one device. Redirection needed to backups")
            session.devid = int(list(userdata['devices'])[0])
            bkp = backup.Backup(srvr_db)
            bkplist = bkp.get_backup_list_for_client(userdata['id'])
            for k,v in bkplist.items():
                if v.get('settings', {}).get('folder',{}).upper().startswith('DESKTOP'):
                    #from backups find if a folder name desktop exists somewhere
                    macroList = srvr_db.get_macro_location_list('DESKTOP',session.devid )
                    if macroList and len(macroList)==1:
                        bkplist[k]['settings']['folder'] = macroList[0]+v.get('settings', {}).get('folder', {})[7:].replace('\\', '/')
                    else:
                        if macroList:
                            #Multiple folder have name Desktop/Documents etc. SO we are confused now.
                            #Show device list instead now.
                            break
                        #else:
                            #Probably no backup yet from that location. So same state.
            if bkplist:
                devlist=None #Do not display devices now. since this is only device. SO we will show backup.
            del bkp
        if not userdata['devices']:
            devlist = None
        return client_render.client_layout(client_render.userhome(devices=devlist, backup=bkplist), username=userdata['name'], shortcuts=userdata['shortcuts'])

class adminhome:
    @validate_admin
    def GET(self, search=None):
        usrs = myusers.get_users_list(search=search)
        licens = myusers.get_licenses()
        return answer(render.adminhome, users=usrs, devices={}, licenses=licens, msg="")

    @validate_admin
    def POST(self):
        i = web.input()
        if i.get("search") is not None:
            return self.GET(i.get("search"))

        uid = int(i.uid)
        userdata = myusers.get_user_details(uid)
        act = i.get('act', 'None')
        if act == 'None':
            #Add user is a probability. So send to register
            return register.POST(self)
        elif act == 'get_device_list':
            return answer(render.adminhome, users={uid:userdata['name']}, devices=userdata['devices'], msg="")
        elif act == 'get_folder':
            session.uid = uid
            session.devid = int(i.device)
            return answer(web.redirect, redirect_to='/filelist/')
        elif act == 'set_target':
            if i.target == "":
                return self.GET()
            srvr_db.create_policy(session.uid, i.target)
            return answer(web.redirect, redirect_to='adminhome')
        elif act in ['pending', 'allow', 'deny', 'edit', 'revoke', 'delete']:
            if act != 'edit':
                id = i.uid
                if id == 0:
                    return answer(web.redirect, redirect_to='adminhome')
                act = i.act
                myusers.set_user_state(id, act)
                return answer(web.redirect, redirect_to='adminhome')

class reporting:
    @validate_admin
    def GET(self):
        rpt = reports.Reports()
        reports_list = rpt.get_list()
        user_list = myusers.get_users_list(type_of_user='user')
        return answer(render.reports, reports=reports_list, msg='', users=user_list)

    def POST(self):
        i = web.input()
        rpt = reports.Reports()
        #TODO: process input here
        param = {}
        if not i.get('repeat'):
            param['repeat'] = 'no'
        else:
            param['repeat'] = i.repeat
        param['scope'] = i.scope
        param ['sendto'] = i.sendto
        param['ato'] = i.ato


        rpt.add_report(param)
        reports_list = rpt.get_list()
        user_list = myusers.get_users_list(type_of_user='user')
        return answer(render.reports, reports=reports_list, msg='', users=user_list)

class backup_router:
    def GET_LIST(self, search=None):
        print("Backup get")
        bkp = backup.Backup(srvr_db)
        if isAdmin():
            backup_list = bkp.get_backup_list(search=search)
            user_list = myusers.get_users_list(type_of_user='user')
            del bkp
            return answer(render.backup, backup=backup_list, users=user_list, msg="")
        else:
            backup_list = bkp.get_backup_list_for_client(session.uid, search=search)
            userdata = myusers.get_user_details(session.uid)
            del bkp
            uid=session['uid']
            shortcutt= userdata['shortcuts']
            retval = answer(render.backupcli, backup=backup_list, uid=uid, shortcuts=shortcutt, msg="")
            return retval
    @validate_user_or_admin
    def GET(self, param):
        print("Get with Param", param)
        i=web.input()
        if param=="":
            return self.GET_LIST()
        bkp = backup.Backup(srvr_db)
        if 'get' in param:
            #Client trying to get backup list for a specific user and device
            ratVal= bkp.get_backup_list_for_client(session['uid'])
            return ratVal
        if isAdmin():
            backup_list = bkp.get_backup_list(i.uname)
        else:
            backup_list = bkp.get_backup_list_for_client(session.uid)
        #user_list = myusers.get_users_list()
        del bkp
        userdata = myusers.get_user_details(session.uid)
        return client_render.client_layout(client_render.backupcli(backup=backup_list, uid=session.uid), username=userdata['name'], shortcuts=userdata['shortcuts'])
        # return backup_list

    @validate_user_or_admin
    def POST(self, ext):
        print("Backup post")
        i = web.input()
        if 'search' in i:
            return self.GET_LIST(search=i['search'])
        bkp = backup.Backup(srvr_db)
        try:
            option = i.backup_options
        except:
            print("No backup option provided.")
            option = None
            return ""

        if isAdmin():
            user_list = myusers.get_users_list()
            renderbackup = render.backup
            shortcuts = []
            success_msg = "Added Successfully"
        else:
            user_list = []
            renderbackup = render.backupcli
            shortcuts = session.shortcuts
            success_msg = "Added Successfully. Ask your admin to enable it."

        if isAdmin() and (option in (BACKUP_ACTIVE, BACKUP_INACTIVE, BACKUP_REMOVED)):
            idbkp = i.iD
            try:
                if bkp.set_backup_state(idbkp, option):
                    backup_list = bkp.get_backup_list()
                    return answer(renderbackup, backup=backup_list, users=user_list, msg="Action Completed Successfully!!")
                raise ValueError('')
            except:
                i.param = ""

        if isAdmin() and (option == 'edit'):
            #TODO: show the user's backup saved
            backup_list = bkp.get_backup_list()
            return answer(renderbackup, backup=backup_list, users=user_list, msg="Action Completed Successfully!!")

        #Client delete backup case
        if not isAdmin() and (option == BACKUP_REMOVED):
            bkp.remove_backup(i.iD)
            return answer(web.redirect, redirect_to='backupcli')

        #if we are here, then it is either client call to get backup list or a new backup creation by user or admin
        print("Opted to have a new backup of %s type" % option)
        try:
            print(i.param)
        except:
            i.param = ""
        backup_list = bkp.get_backup_list()
        if i.param == "":
            if isAdmin():
                return answer(renderbackup, backup=backup_list,users=user_list, msg="Blank Path provided. Provide path properly")
        params = {}
        params["type"] = option
        params['folder'] = i.param
        params['filters'] = i.filters[:-1]
        params['skip']   = i.skip
        params['start'] = i.strt
        params['end'] = i.endt
        retval = None
        try:
            if option == 'group':
                retval = bkp.create_backup(i.grp, params)
            else:
                if isAdmin():
                    if (i.user_list_select != 'select_one'):
                        retval = bkp.create_backup(i.user_list_select, params, True)
                else:
                    retval = bkp.create_backup(session.uid, params, False)
        except:
            traceback.print_exc()
            return answer(renderbackup, backup=backup_list,users=user_list, msg="Improper values provided", shortcuts=shortcuts, uid=session.uid)

        # get the latest list again
        if isAdmin():
            if retval is None:
                success_msg = "Error in adding the backup"
            return answer(renderbackup, backup=bkp.get_backup_list(), users=user_list, msg=success_msg, shortcuts=shortcuts,uid=session.uid)
        else:
            userdata = myusers.get_user_details(session.uid)
            return client_render.client_layout(client_render.backupcli(backup=bkp.get_backup_list_for_client(session.uid), uid=session.uid),username=userdata['name'], shortcuts=userdata['shortcuts'])

class policy:
    @validate_admin
    def POST(self):
        #Validate its client
        i = web.input()
        #TODO: Encrypt and send data from client and decrypt here
        # identifier = i.signature
        # enc_req_data = i.data
        policies = srvr_db.get_policies(session.uid)
        return answer(None, policies=policies)

class settings:
    def GET(self, msg=""):
        config = init_config()
        try:
            smtp = config.items('SMTP')
        except:
            smtp = {'hostname':'', 'port':'', 'email':'', 'password':'', 'tls':'False'}
        smtp = dict(smtp)
        smtp['password'] = 'dummy_password' #Do not send it back to UI. Just take and change, if changed.
        return answer(render.setting, server=config.items('SERVER'), smtp=smtp, msg=msg, ver=config.items('VERSION_CONTROL'))

    def POST(self):
        i = web.input()
        global config
        new_config = configparser.ConfigParser()
        new_config['DEFAULT'] = {   'max_file_size':i.max_file_size,
                                'split_for_size':i.split_for_size,
                                'download_for_count':i.download_for_count,
                                'download_for_size':i.download_for_size
                            }
        new_config['SERVER'] = {}
        server = new_config['SERVER']
        server['port'] = i.port  # mutates the parser
        server['cache_clean_after']= i.cache_clean_after
        new_config['TEMP'] = dict(set(config.items('TEMP'))-set(config.items('DEFAULT')))
        new_config['LOG'] = dict(set(config.items('LOG'))-set(config.items('DEFAULT')))
        passwd = i.smtp_pwd if  i.smtp_pwd  != 'dummy_password' else config.get('SMTP', 'password')
        tls = True if i.get('smtp_tls') == 'on' else False
        new_config['SMTP'] = {
                                'hostname':i.smtp_hostname,
                                'port':i.smtp_port,
                                'email':i.smtp_email,
                                'password':passwd,
                                'tls':tls
                            }
        try:
            new_config['VERSION_CONTROL'] = {
            'Max_allowed_versions' : int(i.min_ver),
            'version_clean_interval': int(i.ver_clean_wake_time)
            }
        except:
            new_config['VERSION_CONTROL'] = dict(set(config.items('VERSION_CONTROL'))-set(config.items('DEFAULT')))

        with open(os.path.join(CONFIG_DIR, CONFIG_FILE), 'w') as configfile:
            new_config.write(configfile)
            configfile.close()
        return answer(web.redirect, redirect_to='settings')

class chunk:
    def POST(self):
        #Is it chunk check or chunk put?
        #if chunk get:
        i = web.input()
        if i.type == 'check_hash':
            hash_list = i.data.split(",")
            ret_hash = []
            for h in hash_list:
                if not srvr_db.check_chunk_hash(h):
                    ret_hash.append(h)
            return ",".join(ret_hash)
        if i.type == 'meta':
            #srvr_db.get_db().files.insert_one({'filepath': i.data['nm'] , 'actual_path': , 'version': i.data['version'],\
            # 'chunks_count': i.data['chunks_count'], 'hashes': , 'starts': i.data['starts'], 'ends': i.data['ends']})
            #filesize = i.data['ends'][-1] - i.data['starts'][0]
            #dedup_chk = True
            #Discussed with Divs. Will Store information in separate DB for now
            #Will transfer to main files table when upload is over
            #srvr_db.query_and_create_path_entries(i.data['actual_path'], i.data['hashes'], devid, i.data['version'], filesize, dedup_chk)
            hashes_needed = srvr_db.file_to_upload(i.data, session.devid)
            return hashes_needed
        if i.type == 'commit':
            hashes = srvr_db.get_hash_list(i.data, session.devid)
            if hashes == None:
                # We are missing one or more hashes. Drop this file for now. Let client retry.
                return False
            data = json.loads(i.data)
            srvr_db.query_and_create_path_entries(data['nm'],hashes ,session.devid, data['version'], data['filesize'])
            srvr_db.del_hash_list(data, session.devid)
            return True
        if i.type == b'chunk_data':
            c_hash = i.hsh.decode('utf-8')
            chunksize = int(i.sz.decode('utf-8'))
            filedir = get_save_to_path()
            chunkdir = os.path.join(filedir, c_hash[0],c_hash[1],c_hash[2],c_hash[3],c_hash[4])
            chunkpath = os.path.join(chunkdir, c_hash)
            os.makedirs(chunkdir,  exist_ok=True)
            fout = open(chunkpath, 'wb')  # creates the file where the uploaded file should be stored
            x = web.input(files={})
            fout.write(i.files)  # writes the uploaded chunk to the newly created file.
            fout.close()
            cst = os.stat(chunkpath)
            if cst.st_size == chunksize:
                c_idx = srvr_db.add_hash(c_hash, chunksize)
                srvr_db.queue_file_for_upload(c_idx, c_hash)
                return True
            else:
                return False

        unknown_chunks = []
        for x in i.data:
            if not srvr_db.check_chunk_hash(x):
                unknown_chunks.append(x)
        return unknown_chunks

class ldap:
    @validate_admin
    def GET(self):
        global ldapwrapper
        if web.input() == {}:  #means simple get page request
            cmn.ldapobj = lp = ldapwrapper
            settings = lp.get_ldap_settings(None)
            return answer(render.ADsettings, settings = settings)
        else:  #means to get a specific ldap setting details
            return json.dumps(ldapwrapper.get_ldap_settings(str(web.input()['name'])))

    @validate_admin
    def POST(self):
        i = web.input() if web.input() != {} else json.loads(web.data())
        if str(i['method']) == 'add':
            ldapwrapper.add_ldap_settings(json.dumps(i))
        elif str(i['method']) == 'del':
            ldapwrapper.delete_ldap_settings(i['name'])
        return answer(web.redirect, redirect_to='ldap')

class edit:
    @validate_admin
    def POST(self):
        i = web.input()
        if str(i['type']) == 'user':
            if str(i['password']) == '':
                myusers.updateUser(i)
            else:
                pwd_hash = hashlib.md5(str(i['password']).encode('utf-8')).hexdigest()
                myusers.updateUser(i, pwd_hash)
            return answer(web.redirect, redirect_to='adminhome')
        elif str(i['type']) == 'ADsetting':
            ldapwrapper.edit_ldap_settings(json.dumps(i))
            return answer(web.redirect, redirect_to='ldap')
        elif str(i['type']) == 'group':
            old_group = srvr_db.get_groups(int(i['gid']))
            if old_group['name'] != i['gname']:
                srvr_db.groups.update({'gid': int(i['gid'])}, {'$set': {'name': str(i['gname'])}})
            user_list = i['userlist'].split(',')[:-1]
            user_list = [int(j) for j in user_list]
            srvr_db.groups.update({'gid': int(i['gid'])}, {'$set': {'members': user_list}})
            return answer(web.redirect, redirect_to='groups')
        elif str(i['type']) == 'backup':
            bkp = backup.Backup(srvr_db)
            bkp.edit_backup(i)
            return answer(web.redirect, redirect_to='backup')

class groups:
    @validate_admin
    def GET(self):
        global ldapwrapper
        if web.input() == {}:
            return answer(render.groups, groups = srvr_db.get_groups(None), users = myusers.get_users_list(), settings=ldapwrapper.get_ldap_settings(None))
        else:
            members = srvr_db.get_groups(int(web.input()['gid']))['members']
            members = [int(i) for i in members]
            users = myusers.get_users_list()
            for i in users:
                i['selected'] = True if i['id'] in members else False
            return json.dumps(users)

    @validate_admin
    def POST(self):
        i = web.input()
        if str(i['action']) == 'add':
            user_list = i['userlist'].split(',')[:-1]
            user_list = [int(j) for j in user_list]
            max_id = int(str(srvr_db.get_groups(None, True)))
            data = {"gid":max_id+1, "name":str(i['gname']), "mode":str(i['mode']), "members":user_list}
            if str(i['mode']) == 'ldap':
                data['ldap_setting'] = str(i['ldap_setting'])
            srvr_db.add_group(data)
        elif str(i['action']) == 'del':
            srvr_db.delete_group(int(i['gid']))
        return answer(web.redirect, redirect_to='groups')

class base_dn:
    @validate_admin
    def GET(self):
        i = web.input()
        return json.dumps({"base__dn":str(ldapwrapper.get_base_dn(str(i['ip']), int(i['port'])))})

    @validate_admin
    def POST(self):
        i = json.loads(web.data())
        return json.dumps({"status": ldapwrapper.test_connection(domain=i['ip'], username=i['username'], password=i['password'], port=int(i['port']))})

class imports:
    @validate_admin
    def GET(self):
        i = web.input()
        groupList = ldapwrapper.get_groups_name(str(i['name']))
        return json.dumps({'list': groupList})

    def POST(self):
        i = web.input()
        ldapwrapper.import_group(i['gname'], i['sname'])

class backup_info:
    def POST(self):
        i = json.loads(web.data())
        print(str(i))
        try:
            bk = backup.Backup(srvr_db)
            return json.dumps(bk.get_backup_by_id(int(i['id'])))
        except:
            return answer(web.redirect, redirect_to='backup')

##############################################################
#
#   M A I N
#
##############################################################
class GracefulKiller:
  kill_now = False
  def __init__(self):
    signal.signal(signal.SIGINT, self.exit_gracefully)
    signal.signal(signal.SIGTERM, self.exit_gracefully)

  def exit_gracefully(self,signum, frame):
    self.kill_now = True
    log.info("Server Stopped...Exiting.")
    log.close()

if __name__ == "__main__":



    killer = GracefulKiller()
    DEBUG = False
    if "debug" in sys.argv:
        DEBUG = True

    #Configure logging
    global logstarted

    try:
        logstarted
    except:
        logstarted = False
    if not logstarted:
        log = mylog.log
        print("ALL OUTPUT DIVERTED TO ",
              os.path.join(config.get(LOG_CONFIG_KEY, LOG_LOCATION_CONFIG_KEY), LOG_FILENAME))
        log.set_log_file_name(os.path.join(config.get(LOG_CONFIG_KEY, LOG_LOCATION_CONFIG_KEY), LOG_FILENAME))
        log.set_console(sys.stdout)
        log.start()
        log.info("Service Started...")
        stdout = sys.stdout
        console = sys.stdout
        logname = "server"
        sys.stdout = log
        sys.stderr = sys.stdout
        logstarted = True

    # MAIN SERVER LOOP
    if True: #May be replaced with while True in future, if we break such that server need to restart after an exception
        try:
            with open(PID_FILE, "w") as f:
                f.write(str(os.getpid()))
            app.run()
        except:
            traceback.print_exc()
            log.error("Unexpected error:", sys.exc_info()[0])
