"""
This file connects to the db. After connection, checks the groups list. If any group is imported from the AD, fetches
it's members and then imports the members accordingly.
"""

__author__ = 'dc'
__copyright__ = 'Copyright 2018, SimpleBackup'

import dbwrapper
import user
from ldap3 import NTLM, ALL, Connection, Server, ALL_ATTRIBUTES


class ldapService:
    def __init__(self):
        # Create DB instance here with default values
        self.db = dbwrapper.ServerDB()

    def import_users(self):
        # create a time poller here
        result = self.db.get_groups(None)
        for i in result:
            if str(i.get('type', '')) == 'ldap':
                self.get_members(i['setting_name'], i['gname'])
        # go to sleep again

    def get_members(self,setting_name, group_name):
        setting = self.db.get_ldap_settings(setting_name)
        server = Server(setting['IP'], get_info=ALL)
        conn = Connection(server, user=setting['manager_dn'], password=setting['password'], authentication=NTLM)
        try:
            if conn.bind():
                search_filter = '&((objectClass=Group)(name=%s))', str(group_name)
                # this will return a list with the member details
                conn.search(str(setting['base_dn']), search_filter=search_filter, attributes=['member'])
                result = list(conn.entries)
                for i in result:
                    # iterate over the list and get the user details
                    conn.search(search_base=i, search_filter='(&(objectClass=person))', attributes=ALL_ATTRIBUTES)
                    if len(conn.entries) == 1:
                        self.save_user(conn.entries[0])
                conn.unbind()
            else:
                pass
        except:
            pass

    def save_user(self, user_details):
        data = dict()
        data['cn'] = str(user_details.cn)
        data['name'] = str(user_details.givenName)
        data['distinguishedName'] = str(user_details.distinguishedName)
        data['sAMAccountName'] = str(user_details.sAMAccountName)
        data['email'] = str(user_details.userPrincipalName)
        myusers = user.User(self.db.get_db())
        try:
            myusers.add_user(None, None, None, ldap_details=data)
        except:
            pass
