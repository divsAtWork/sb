from common import *
from mylog import *
import sys
import time, os
import dbwrapper
import traceback
import signal

class Cleaner:
    global log
    def __init__(self):
        try:
            self.srvr_db = dbwrapper.ServerDB()
            self.db = self.srvr_db.get_db()
            #self.config = configparser.ConfigParser(os.path.join(CONFIG_DIR, CONFIG_FILE))
            self.clean_after_period = config.get('SERVER','clean_cache_after')
            self.keep_processing = True
        except IOError as e:
            log.error("I/O error({0}): {1}".format(e.errno, e.strerror))
        except ValueError:
            log.error("Could not convert data to an integer.")
        except:
            log.error("Unexpected error:", sys.exc_info()[0])
            raise

    # Remove files from system after certain period.
    # Idea is that if a chunk is stored for long time, move it to secondary storage and remove from
    # primary storage. Example - Admin can set that keep data in local system only for 3 days and then
    # move the data to cloud. This would reduce requirement of local storage. If someone do not want to delete
    # from local storage, they can set this value (clean_after_period) to be very high (like 20 years).
    def run(self):
        lastrun = 0
        try:
            cur =  self.db.systemsettings.find_one({},{"clean_cache_after_last_run":1})
            id = cur['_id']
            val = cur['clean_cache_after_last_run']
            lastrun = int(val)
        except:
            self.db.systemsettings.insert({"clean_cache_after_last_run": 0})
        curTime = int(time.time())
        cleanTime = int(self.clean_after_period) * 24*60*60 # Days to seconds
        if (curTime - lastrun) > cleanTime:
            #Let's clean
            for root, dirs, files in os.walk(get_save_to_path(), topdown=False):
                for name in files:
                    stat = os.stat(os.path.join(root, name))
                    if (curTime - stat.st_ctime) > cleanTime:
                        os.remove(os.path.join(root, name))
            #Finally save the time this was run
            self.db.systemsettings.update({},{"$set":{"clean_cache_after_last_run":curTime}})


##############################################################
#
#   M A I N
#
##############################################################
class GracefulKiller:
    kill_now = False
    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self,signum, frame):
        self.kill_now = True
        log.info("Server Stopped...Exiting.")
        log.close()

if __name__ == "__main__":
    killer = GracefulKiller()
    DEBUG = False
    if "debug" in sys.argv:
        DEBUG = True
    cleaner = Cleaner()
    while 1:
        try:
            cleaner.run()
            time.sleep(60*60) # try again after an hour
        except:
            traceback.print_exc()
            log.error("Unexpected error:", sys.exc_info()[0])