from common import *
from mylog import *
import sys
import time,datetime
import dbwrapper
import traceback
import signal

class Cleaner:
    global log
    def __init__(self):
        try:
            global config
            self.srvr_db = dbwrapper.ServerDB()
            self.db = self.srvr_db.get_db()
            self.clean_after_period = config.get('VERSION_CONTROL','version_clean_interval')
        except IOError as e:
            log.error("I/O error({0}): {1}".format(e.errno, e.strerror))
        except ValueError:
            log.error("Could not convert data to an integer.")
        except:
            log.error("Unexpected error:", sys.exc_info()[0])
            raise

    def run(self):
        '''
        Clean the older versions of a file. The idea is to keep a limited defined
        number of versions of a file and removed the extra ones. For example, if the admin
        decides to keep 5 minimum versions per file, no file should have any 6th or later version
        present in the database.
        This cleaner runs after every version_clean_interval * 60 minutes.
        '''
        dt = datetime.datetime()
        print('Starting the cleaning process at: [%s] %dt.now()')
        cur =  self.db.file_cleaner_queue.find({})
        for doc in cur:
            try:
                for hash in doc['hsh']:
                    self.srvr_db.del_hash(hash)
                self.db.files.remove(doc)
            except:
                print("Error while deleting version data for document: [%s] %doc")
        print('Sleeping the version control cleaner at: [%s] %dt.now()')

##############################################################
#
#   M A I N
#
##############################################################
class GracefulKiller:
    kill_now = False
    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self,signum, frame):
        self.kill_now = True
        log.info("Server Stopped...Exiting.")
        log.close()

if __name__ == "__main__":
    killer = GracefulKiller()
    DEBUG = False
    if "debug" in sys.argv:
        DEBUG = True
    cleaner = Cleaner()
    while 1:
        try:
            cleaner.run()
            time.sleep(60*60) # try again after an hour
        except:
            traceback.print_exc()
            log.error("Unexpected error:", sys.exc_info()[0])