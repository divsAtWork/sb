from pymongo import MongoClient
from common import *
from mylog import *
import sys
import s3wrapper
import user
import time, os
import mylog
from shutil import copyfile
import storewrapper
import threading
import dbwrapper

class Upload:
    global log
    def __init__(self):
        try:
            #db = MongoClient('localhost', 27017)
            self.srvr_db = dbwrapper.ServerDB()
            self.db = self.srvr_db.get_db()
            self.user = user.User(self.db)
            self.s3 = s3wrapper.My_S3()
            self.storagelocations = []
            self.storageinitialized = False
            self.keep_processing = True
        except IOError as e:
            log.error("I/O error({0}): {1}".format(e.errno, e.strerror))
        except ValueError:
            log.error("Could not convert data to an integer.")
        except:
            log.error("Unexpected error:", sys.exc_info()[0])
            raise

    def init_storage(self):
        if self.storageinitialized: return True
        result = self.db.storage.find({"stat": "active"})
        for one_storage in result:
            if one_storage['settings']['type'] == 'cifs':
                #connect to the location
                log.info("Should have cifs storage")
            if one_storage['settings']['type'] == 'disk':
                log.info("Disk type of mapping")
                self.storagelocations.append((one_storage['_id'], storewrapper.Folder(one_storage['settings']['folder'])))
                #self.storagelocations['pointer'] = Folder(one_storage['settings']['folder'])

    def close_storage(self):
        #Write here is there is any closure required to the mount points
        self.storageinitialized = False


    def get_one_file_from_queue(self, cur=None):
        error = 0
        #TODO: Try each file more than once. But give first chance to every file
        #then increase the error count.
        if cur == None:
            try:
                cur =  self.db.uploadqueue.find_one() #{"upd":{"$ne":1}}, {"$set":{"upd":1}})
                return cur #, cur.next()
            except:
                return None #, {}
        else:
            try:
                return cur #, cur.next()
            except:
                return None #, {}


    # Design assumption: tmp_filepath is unique per file to be uploaded. Should therefore include
    # path, userid, device id, timestamp and a small random value.
    # File should be left in the temp folder until it is uploaded to all the storage locations
    # wherever upload is successful, it should be added in dict
    def DEPRECATED_process_files_queue_for_upload_to_storage(self, hsh):
        error = False
        for storage_id,pointer in self.storagelocations:
            result = self.db.hashtable.find_one({"_id":hsh})
            found = False
            for aresult in result:
                print(aresult['store'])
                #devID=aresult['devid']
                #Maybe file is not left for this storage
                if storage_id in aresult['store']:
                    found = True
                    break
            if found: continue
            try:
                filesize=pointer.save(devID, actual_filepath, tmp_filepath)
                #retval = self.db.files.find_one({'tmpPath':tmp_filepath},{'_id':1, "store":1})
                self.db.files.update({'_id':aresult['_id']},{'$set':{'store':aresult["store"]+":"+str(storage_id)}})
                self.db.storage.update({'_id':storage_id}, {'$inc':{"settings.usage":filesize}})
            except:
                log.error("Error in uploading the file ",tmp_filepath," to storage",storage_id)
                self.db.uploadqueue.update({'tmp': tmp_filepath, 'act': actual_filepath},{'$inc': {'error': 1}})
                error = True
        if not error:
            self.db.uploadqueue.remove({'tmp': tmp_filepath, 'act': actual_filepath})
            os.remove(self.get_save_to_path()+tmp_filepath)
        return True

    def upload_hash(self, hsh):
        result = self.db.hashtable.find_one({"_id": hsh})
        for storage_id, pointer in self.storagelocations:
            if storage_id in result['store']:
                continue
            else:
                filesize = pointer.save(hsh)
        return self.db.hashtable.update({'_id':hsh}, {'$push':{'store':storage_id}})


    def process_func(self):
        cur = None
        threads = []
        self.allstorages = self.srvr_db.get_storage_list()
        #Keep queue only for active storages
        for aStore in self.allstorages:
            if aStore['stat'] != "active":
                #Drop this entry
                del self.allstorages['aStore']
            else:
            #Create one thread per store queue
                t = threading.Thread(target=self.process_store_upload, args=[aStore['_id'],])
                threads.append(t)
                status = t.start()
                print(status)
        while 1:
            cur = self.db.uploadqueue.find_one()
            if cur:
                for aStore in self.allstorages:
                    log.info("Queing for storage id=", cur['_id'])
                    #self.upload_hash(cur['hash'])
                    sname = "upload_"+str(aStore['_id'])
                    self.db.get_collection(sname).insert({"_id":cur['hash']})
                #Done move for all existing storages. Lets remove this from main queue.
                self.db.uploadqueue.remove({"_id":cur['_id']})
            else:
                if not self.keep_processing:
                    break
                time.sleep(15)


    def process_store_upload(self, store_id):
        store = self.db.storage.find_one({"_id":store_id})
        if store['settings']['type'] == 'disk':
            storePointer = storewrapper.Folder(store['settings']['folder'])
        while 1:
            sname = "upload_"+str(store_id)
            cur = self.db.get_collection(sname).find_one()
            if cur:
                if storePointer.save(cur['_id']):
                    self.db.hashtable.update({'_id':cur['_id']}, {'$push':{'store':store_id}})
                    self.db.get_collection(sname).remove({"_id":cur['_id']})
            else:
                time.sleep(15)


###  MAIN UPLOADER #########

AWSAccessKeyId = "AKIAIK66CNSM2H4OVE5A"
AWSSecretKey = "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"
AWSurl = "s3://choprasanjay.s3.amazonaws.com/myfile.txt"
DEBUG = False
if "debug" in sys.argv:
    DEBUG = True
log = mylog.Log()
log.set_log_file_name('../logs/uploder.log')
log.set_log_level(1)
log.start()
upload = Upload()
upload.init_storage()
upload.process_func()
#InterestingThreadA = threading.Thread(target=upload.process_func, args=())
#InterestingThreadA.start()
#upload.process_func()