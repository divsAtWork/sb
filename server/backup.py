from common import *
import re
import pymongo

class Backup:
    def __init__(self, db):
        print("Backup class inits")
        self.db=db

    def get_backup_list(self, uname=None, search=None):
        try:
            if(uname==None):
                    if search:
                        bkpregx = re.compile(search, re.IGNORECASE)
                        result = self.db.db.backup.find({'stat': {'$ne': BACKUP_REMOVED}, "settings.folder": bkpregx})
                    else:
                        result = self.db.db.backup.find({'stat': {'$ne': BACKUP_REMOVED}})
            else:
                user_ids = []
                regex = re.compile('^'+uname, re.IGNORECASE)
                if search:
                    bkpregx = re.compile(search, re.IGNORECASE)
                    result = self.db.user.find({'fname': regex, "settings.folder": bkpregx})
                else:
                    result = self.db.user.find({'fname': regex})
                for aVal in result:
                    user_ids.append(aVal['_id'])
                result = self.db.db.backup.find({'stat': {'$ne': BACKUP_REMOVED}, 'type':'user', 'ato':{'in':user_ids}})
            if result != None:
                backup_set = {}
                for anItem in result:
                    backup_set[str(anItem['_id'])] = anItem
                return backup_set
            else:
                return {}
        except:
            return {}

    def get_backup_list_for_client(self, id, devid=0):
        retval = []
        result = self.db.db.backup.find({'stat': BACKUP_ACTIVE, 'ato':str(id)})
        if result != None:
            backup_set = {}
            for anItem in result:
                backup_set[str(anItem['_id'])] = anItem
            return backup_set
        else:
            return {}

    def create_backup(self, idbkp, params, isAdmin):
        result = None
        if (params != None) and (params!= {}):
            try:
                list_id = list(self.db.db.backup.find().sort('_id', pymongo.DESCENDING).limit(1))
                id = int(list_id[0]['_id'])+1
            except:
                id = 1
            for fldr in params['folder'].split(";"):
                fldr = fldr.strip()
                abc = fldr.lower()
                if abc not in ['', "c:", 'c:\\', 'c:/'] and not abc.startswith("http"):
                    settings = {'folder':fldr, 'filters':params['filters'], 'skip':params['skip'], 'type':params['type']}
                    timers = {'start': params['start'], 'end': params['end']}
                    stat = BACKUP_INACTIVE if isAdmin else BACKUP_ACTIVE
                    result = self.db.db.backup.insert_one({'_id':id, 'ato':str(idbkp), 'settings':settings, 'stat':stat, 'timers': timers})
                    id +=1
        return result

    def set_backup_state(self, id, newState):
        if newState == BACKUP_REMOVED:
            return self.remove_backup(id)
        result = self.db.db.backup.update({'_id': int(id)}, {'$set':{'stat': newState}})
        if result == None:
            return False
        return True

    def remove_backup(self, id):
        return self.db.db.backup.remove({'_id':int(id)})

    def get_backup_by_id(self, id):
        return self.db.db.backup.find_one({'_id':int(id)})

    def edit_backup(self, web_input):
        try:
            prev_result = self.get_backup_by_id(str(web_input['backup_id']))
            if str(prev_result['timers']['start']) != str(web_input['estrt']):
                self.db.db.backup.update({"_id": int(str(web_input['backup_id']))}, {'$set': {'timers.start': str(web_input['estrt'])}})
            if str(prev_result['timers']['end']) != str(web_input['eendt']):
                self.db.db.backup.update({"_id": int(str(web_input['backup_id']))}, {'$set': {'timers.end': str(web_input['eendt'])}})
            if str(prev_result['settings']['folder']) != str(web_input['eparam']):
                self.db.db.backup.update({"_id": int(str(web_input['backup_id']))}, {'$set': {'settings.folder': str(web_input['eparam'])}})
            if str(prev_result['settings']['skip']) != str(web_input['eskip']):
                self.db.db.backup.update({"_id": int(str(web_input['backup_id']))}, {'$set': {'settings.folder': str(web_input['eskip'])}})
            return True
        except:
            return False